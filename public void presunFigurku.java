
    public void presunFigurku(FigurkaD f, int sloupec, int radek, int priorita/**/) {

        boolean t = Math.abs(f.startX - radek) == Math.abs(f.startY - sloupec);

        if (f.druh == 0) {
            this.hraci[radek/*-1*/][sloupec/*-1*/] = new FigurkaD(f.startX, f.startY, f.pohyb, f.barva, f.druh);
            if (f.barva == 0 && priorita == 1) {
                if (f.startY == sloupec - 1 || f.startY - 1 == sloupec) {
                    this.hraci[f.startX][f.startY] = null;
                    this.figurkaNaPoli(sloupec, radek).startX = radek;
                    this.figurkaNaPoli(sloupec, radek).startY = sloupec;
                } else if (/*f.startX > radek-1 && kolo ==0*/(f.startY < sloupec)) {

                    this.hraci[ f.startX - 1][f.startY + 1] = null;
                    this.hraci[f.startX][f.startY] = null;
                    this.figurkaNaPoli(sloupec, radek).startX = radek;
                    this.figurkaNaPoli(sloupec, radek).startY = sloupec;

                } else if (f.startY > sloupec)/*(f.startX > radek-1 && kolo >0)*/ {
                    // if (t == true) {
                    this.hraci[ f.startX - 1][f.startY - 1] = null;
                    this.hraci[f.startX/*-2*/][f.startY] = null;

                    this.figurkaNaPoli(sloupec, radek).startX = radek;
                    this.figurkaNaPoli(sloupec, radek).startY = sloupec;

                }

            } else if (f.barva == 1 && priorita == 1) {

                if (f.startY == sloupec - 1 || f.startY - 1 == sloupec) {
                    this.hraci[f.startX][f.startY] = null;
                    this.figurkaNaPoli(sloupec, radek).startX = radek;
                    this.figurkaNaPoli(sloupec, radek).startY = sloupec;
                } else if (f.startY > sloupec) {
                    this.hraci[ f.startX][f.startY /*- 2*/] = null;
                    this.hraci[f.startX + 1][f.startY - 1] = null;
                    this.figurkaNaPoli(sloupec, radek).startX = radek;
                    this.figurkaNaPoli(sloupec, radek).startY = sloupec;

                } else if (f.startY < sloupec) {
                    this.hraci[ f.startX][f.startY] = null;
                    this.hraci[f.startX + 1][f.startY + 1] = null;
                    this.figurkaNaPoli(sloupec, radek).startX = radek;
                    this.figurkaNaPoli(sloupec, radek).startY = sloupec;

                }

            } else {
                this.hraci[f.startX][f.startY] = null;
                this.figurkaNaPoli(sloupec, radek).startX = radek;
                this.figurkaNaPoli(sloupec, radek).startY = sloupec;

            }
            //cerna = 1, bila = 0
            if (f.barva == 1 && radek == 7 && (sloupec == 0 || sloupec == 2 || sloupec == 4 || sloupec == 6)) {
                this.hraci[radek][sloupec] = new FigurkaD(f.startX, f.startY, f.pohyb, f.barva, 1);
                //f.druh=1;
            } else if (f.barva == 0 && radek == 0 && (sloupec == 1 || sloupec == 3 || sloupec == 5 || sloupec == 7)) {
                this.hraci[radek][sloupec] = new FigurkaD(f.startX, f.startY, f.pohyb, f.barva, 1);
                // f.druh=1;
            }
        } else if (f.druh == 1) {
            int x = f.startX, y = f.startY;

            if (f.startX < radek) {
                if (f.startY < sloupec) {
                    while (hraci[x + 1][y + 1] == null) {
                        x++;
                        y++;
                    }
                    x++;
                    y++;
                    
                    this.hraci[ x][y] = null;
                    this.hraci[f.startX][f.startY] = null;
                    
                    x = x + 1;
                    y = y + 1;

                } else if (f.startY > sloupec) {
                    while (hraci[x + 1][y - 1] == null) {
                        x++;
                        y--;
                    }
                    x++;
                    y--;
                    this.hraci[ x][y] = null;
                    this.hraci[f.startX][f.startY] = null;
                    x = x + 1;
                    y = y - 1;
                } else {
                    this.hraci[f.startX][f.startY] = null;
                    this.figurkaNaPoli(sloupec, radek).startX = radek;
                    this.figurkaNaPoli(sloupec, radek).startY = sloupec;

                }
            } else if (radek < f.startX) {
                
                if (f.startY < sloupec) {
                    while (hraci[x - 1][y + 1] == null) {
                        x--;
                        y++;
                    }
                    x--;
                    y++;
                    this.hraci[ x][y] = null;
                    this.hraci[f.startX][f.startY] = null;
                    x = x - 1;
                    y = y + 1;

                } else if (f.startY > sloupec) {
                    while (hraci[x - 1][y - 1] == null) {
                        x--;
                        y--;
                    }
                    x--;
                    y--;
                    this.hraci[ x][y] = null;
                    this.hraci[f.startX][f.startY] = null;
                    x = x - 1;
                    y = y - 1;
                } else {
                    this.hraci[f.startX][f.startY] = null;
                    this.figurkaNaPoli(sloupec, radek).startX = radek;
                    this.figurkaNaPoli(sloupec, radek).startY = sloupec;

                }

            }
            this.hraci[x/*-1*/][y/*-1*/] = new FigurkaD(x, y, f.pohyb, f.barva, f.druh);
        }

    }