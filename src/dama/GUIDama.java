/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dama;

import java.io.*;
import dama.*;
import java.awt.Image;
import java.awt.event.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author MrCvok
 */
public class GUIDama extends javax.swing.JFrame implements Runnable {

    private Ovlad ovlad = new Ovlad();
    private HraciPlocha sachovnice = new HraciPlocha();
    private Image obrazek;
    private boolean zadani = true;
    private String vypisy = "";
    private Scanner scan = new Scanner(System.in);// nacteni;
    FigurkaD[][] hraci = new FigurkaD[8][8];
    static int[][] figurky = new int[24][4];
    static BufferedReader read;
    static String radka, anoNe = "";
    static String[] pole;
    private int i = 0;
    public Client c, c2;
    public Server s;
    public int hrac;

    /**
     * Creates new form GUIDama
     */
    public GUIDama() throws FileNotFoundException, IOException, InterruptedException {
        this.setVisible(true);
        initComponents();
      //  do {
            jB_novaHra.setVisible(false);
            se.setVisible(false);
            cl.setVisible(false);
            sitovaHra.setVisible(false);
            jB_Ano.setVisible(false);
            jB_Ne.setVisible(false);
            jB_Konec.setEnabled(false);
            int i = 0;
            ArrayList<String> skok;
            boolean preskoc = false, predcasnyKonec = false;
            int p = 0;
            while (true) {

                if (sitovaHra.isEnabled() == false || mistniHra.isEnabled() == false) {
                    break;
                }
                this.repaint();
            }
            if (mistniHra.isEnabled() == false) {
                jL_T.setText(ovlad.stat());
                ovlad.startHry();
                ovlad.startZaznamuHry();

                while (anoNe == "") {
                    anoNe = nacistHru();
                    this.repaint();
                }
            } else if (sitovaHra.isEnabled() == false) {
                if (!se.isEnabled()) {
                    runserver();
                }
                if (!cl.isEnabled()) {
                    runclient();
                }
            }
            this.i = ovlad.nacteniHry(anoNe);
            int test = 0;
            do {
                /* if (hrac != hraje()&&sitovaHra.isEnabled() == false)*/ {
                    if (hrac == 1) {
                        jB_Konec.setEnabled(false);
                    } else {
                        jB_Konec.setEnabled(true);
                    }
                    // runserver();
                    ovlad.poziceVynul();
                    ovlad.vypisPole();
                    //        ovlad.saveHry();
                    vypisy += "Hraje " + ovlad.hrac[ovlad.hracJ] + "\n";
                    jL_T.setText(vypisy);
                    vlozObrazky();
                    if (!preskoc) {
                        if (!predcasnyKonec) {
                            skok = ovlad.proverOkoli(sachovnice.hrac);
                            boolean pokr = false;
                            int pokracujInt = 0;
                            nastavEnableTrue();
                            boolean konec = false;
                            vypisy += "Zadejte výchozí pole:\n" + ovlad.getVypis();
                            jL_T.setText(vypisy);
                            do {
                                pokr = true;
                                ovlad.nacteniPrvni = vypisPolozku().toLowerCase();
                                while (pokr && (ovlad.nacteniPrvni.length() != 2
                                        || !ovlad.figurka.platnySloupec(ovlad.nacteniPrvni.charAt(0))
                                        || !ovlad.figurka.platnyRadek(ovlad.nacteniPrvni.charAt(1)))) {
                                    if (!predcasnyKonec) {
                                        if (ovlad.nacteniPrvni != "" && ovlad.nacteniPrvni != "konec") {
                                            vypisy += "Neplatné zadání\nZadejte výchozí pole:\n";
                                            jL_T.setText(vypisy);
                                            nastavEnableTrue();
                                        }
                                        predcasnyKonec = ovlad.predcasnyKonec(ovlad.nacteniPrvni);
                                        ovlad.nacteniPrvni = vypisPolozku().toLowerCase();
                                        if (skok.size() > 0) {
                                            for (String item : skok) {
                                                pokr = item.equalsIgnoreCase(ovlad.nacteniPrvni);
                                                if (pokr) {
                                                    preskoc = pokr;
                                                    break;
                                                } /* */
                                            }
                                            if (!pokr) {
                                                pokracujInt = 1;
                                            } else {
                                                pokracujInt = 2;
                                            } /**/

                                        } else {
                                            pokr = true;
                                        }
                                    } else if (predcasnyKonec) {
                                        break;
                                    }
                                }
                                i = ovlad.vlozeniPrvni(ovlad.nacteniPrvni, pokracujInt);
                                konec = true;
                                nastavEnableTrue();
                                //        c.setInpName(ovlad.nacteniPrvni);
                                ovlad.nacteniPrvni = "";
                                ovlad.nacteniDruhy = "";
                            } while (i != 0 && !predcasnyKonec);
                        }
                    }
                    if (!predcasnyKonec) {
                        nastavEnableTrue();
                        vypisy += "Zadejte cílové pole:\n";
                        jL_T.setText(vypisy);
                        do {
                            ovlad.nacteniDruhy = vypisPolozku().toLowerCase();

                            if (!predcasnyKonec) {
                                while (ovlad.nacteniDruhy.length() != 2
                                        || !ovlad.figurka.platnySloupec(ovlad.nacteniDruhy.charAt(0))
                                        || !ovlad.figurka.platnyRadek(ovlad.nacteniDruhy.charAt(1))) {
                                    if (ovlad.nacteniDruhy != "") {
                                        vypisy += "Neplatné zadání\nZadejte cílové pole:\n";

                                        jL_T.setText(vypisy);
                                        nastavEnableTrue();
                                    }
                                    //  ovlad.nacteniDruhy = ovlad.scan.next().toLowerCase();
                                    ovlad.nacteniDruhy = vypisPolozku().toLowerCase();
                                }
                                i = ovlad.vlozeniDruhe(ovlad.nacteniDruhy, preskoc);
                                nastavEnableTrue();
                                ovlad.nacteniPrvni = "";
                                ovlad.nacteniDruhy = "";
                            } else {
                                i = 0;
                            }
                        } while (i != 0);
                        i = 0;
                        preskoc = ovlad.pokracovani();
                        ovlad.getFigurka();
                        ovlad.saveHry();
                    }
                }
                vypisy = "";
                ovlad.nacteniPrvni = "";
                ovlad.nacteniDruhy = "";
                //    runclient();
            } while (ovlad.vyhra(predcasnyKonec) == 0);
        //    jB_novaHra.setVisible(true);
            jL_T.setText(ovlad.getVypis());//
            ovlad.vypisPoleKonce();
            ovlad.vypisPole();
            vlozObrazky();

      /*      while (true) {
                if (jB_novaHra.isVisible() == false) {
                   
                    break;
                }
            }*/
       // } while (jB_novaHra.isVisible() == false);
    }

    private String nacistHru() {

        if (!jB_Ano.isEnabled()) {
            return "a";
        } else if (!jB_Ne.isEnabled()) {
            return "n";
        }
        return "";
    }

    public void runserver() {
        Thread th = new Thread(this);
        th.start();
    }

    public void run() {
        this.se.setText("Server is running");
        s = new Server(this);
    }

    public void runclient() throws IOException {
        this.cl.setText("Client is running");
        c2 = new Client(hrac);
        c = new Client();

    }

    public void addintoarea(String text) {
        jL_T.setText(jL_T.getText() + text);
    }

    private String vypisPolozku() {
        if (!jB_A8.isEnabled()) {
            return "A8";
        } else if (!jB_C8.isEnabled()) {
            return "C8";
        } else if (!jB_E8.isEnabled()) {
            return "E8";
        } else if (!jB_G8.isEnabled()) {
            return "G8";
        } else if (!jB_B7.isEnabled()) {
            return "B7";
        } else if (!jB_D7.isEnabled()) {
            return "D7";
        } else if (!jB_F7.isEnabled()) {
            return "F7";
        } else if (!jB_H7.isEnabled()) {
            return "H7";
        } else if (!jB_A6.isEnabled()) {
            return "A6";
        } else if (!jB_C6.isEnabled()) {
            return "C6";
        } else if (!jB_E6.isEnabled()) {
            return "E6";
        } else if (!jB_G6.isEnabled()) {
            return "G6";
        } else if (!jB_B5.isEnabled()) {
            return "B5";
        } else if (!jB_D5.isEnabled()) {
            return "D5";
        } else if (!jB_F5.isEnabled()) {
            return "F5";
        } else if (!jB_H5.isEnabled()) {
            return "H5";
        } else if (!jB_A4.isEnabled()) {
            return "A4";
        } else if (!jB_C4.isEnabled()) {
            return "C4";
        } else if (!jB_E4.isEnabled()) {
            return "E4";
        } else if (!jB_G4.isEnabled()) {
            return "G4";
        } else if (!jB_B3.isEnabled()) {
            return "B3";
        } else if (!jB_D3.isEnabled()) {
            return "D3";
        } else if (!jB_F3.isEnabled()) {
            return "F3";
        } else if (!jB_H3.isEnabled()) {
            return "H3";
        } else if (!jB_A2.isEnabled()) {
            return "A2";
        } else if (!jB_C2.isEnabled()) {
            return "C2";
        } else if (!jB_E2.isEnabled()) {
            return "E2";
        } else if (!jB_G2.isEnabled()) {
            return "G2";
        } else if (!jB_B1.isEnabled()) {
            return "B1";
        } else if (!jB_D1.isEnabled()) {
            return "D1";
        } else if (!jB_F1.isEnabled()) {
            return "F1";
        } else if (!jB_H1.isEnabled()) {
            return "H1";
        } else if (!jB_Konec.isEnabled()) {
            return "konec";
        } else {
            return "";
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        se = new javax.swing.JButton();
        cl = new javax.swing.JButton();
        sitovaHra = new javax.swing.JButton();
        mistniHra = new javax.swing.JButton();
        jB_novaHra = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jL_T = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jB_Ano = new javax.swing.JButton();
        jB_Ne = new javax.swing.JButton();
        jB_Konec = new javax.swing.JButton();
        jPHraciPlocha = new javax.swing.JPanel();
        jB_A2 = new javax.swing.JButton();
        jB_A4 = new javax.swing.JButton();
        jB_A6 = new javax.swing.JButton();
        jB_A8 = new javax.swing.JButton();
        jB_B1 = new javax.swing.JButton();
        jB_B3 = new javax.swing.JButton();
        jB_B5 = new javax.swing.JButton();
        jB_B7 = new javax.swing.JButton();
        jB_C2 = new javax.swing.JButton();
        jB_C4 = new javax.swing.JButton();
        jB_C6 = new javax.swing.JButton();
        jB_C8 = new javax.swing.JButton();
        jB_D1 = new javax.swing.JButton();
        jB_D3 = new javax.swing.JButton();
        jB_D5 = new javax.swing.JButton();
        jB_D7 = new javax.swing.JButton();
        jB_E2 = new javax.swing.JButton();
        jB_E4 = new javax.swing.JButton();
        jB_E6 = new javax.swing.JButton();
        jB_E8 = new javax.swing.JButton();
        jB_F1 = new javax.swing.JButton();
        jB_F3 = new javax.swing.JButton();
        jB_F5 = new javax.swing.JButton();
        jB_F7 = new javax.swing.JButton();
        jB_G2 = new javax.swing.JButton();
        jB_G4 = new javax.swing.JButton();
        jB_G6 = new javax.swing.JButton();
        jB_G8 = new javax.swing.JButton();
        jB_H1 = new javax.swing.JButton();
        jB_H3 = new javax.swing.JButton();
        jB_H5 = new javax.swing.JButton();
        jB_H7 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jButton16 = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        jButton19 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();
        jButton23 = new javax.swing.JButton();
        jButton26 = new javax.swing.JButton();
        jButton28 = new javax.swing.JButton();
        jButton30 = new javax.swing.JButton();
        jButton32 = new javax.swing.JButton();
        jButton33 = new javax.swing.JButton();
        jButton35 = new javax.swing.JButton();
        jButton37 = new javax.swing.JButton();
        jButton39 = new javax.swing.JButton();
        jButton41 = new javax.swing.JButton();
        jButton43 = new javax.swing.JButton();
        jButton45 = new javax.swing.JButton();
        jButton47 = new javax.swing.JButton();
        jButton50 = new javax.swing.JButton();
        jButton52 = new javax.swing.JButton();
        jButton54 = new javax.swing.JButton();
        jButton56 = new javax.swing.JButton();
        jButton27 = new javax.swing.JButton();
        jButton24 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jButton31 = new javax.swing.JButton();
        label1 = new java.awt.Label();
        label2 = new java.awt.Label();
        label3 = new java.awt.Label();
        label4 = new java.awt.Label();
        label5 = new java.awt.Label();
        label6 = new java.awt.Label();
        label7 = new java.awt.Label();
        label8 = new java.awt.Label();
        label9 = new java.awt.Label();
        label11 = new java.awt.Label();
        label12 = new java.awt.Label();
        label13 = new java.awt.Label();
        label14 = new java.awt.Label();
        label15 = new java.awt.Label();
        label16 = new java.awt.Label();
        label17 = new java.awt.Label();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        se.setText("s");
        se.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seActionPerformed(evt);
            }
        });

        cl.setText("c");
        cl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clActionPerformed(evt);
            }
        });

        sitovaHra.setText("Síťová hra");
        sitovaHra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sitovaHraActionPerformed(evt);
            }
        });

        mistniHra.setText("Místní hra");
        mistniHra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mistniHraActionPerformed(evt);
            }
        });

        jB_novaHra.setText("Nová hra");
        jB_novaHra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_novaHraActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(mistniHra)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sitovaHra, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(77, 77, 77)
                .addComponent(se)
                .addGap(18, 18, 18)
                .addComponent(cl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 149, Short.MAX_VALUE)
                .addComponent(jB_novaHra, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(se, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sitovaHra, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jB_novaHra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(mistniHra, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(cl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jPanel2.setLayout(new java.awt.BorderLayout());

        jL_T.setColumns(20);
        jL_T.setRows(5);
        jScrollPane1.setViewportView(jL_T);

        jPanel2.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel3.setLayout(new java.awt.BorderLayout());

        jB_Ano.setText("Ano");
        jB_Ano.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_AnoActionPerformed(evt);
            }
        });
        jPanel3.add(jB_Ano, java.awt.BorderLayout.PAGE_START);

        jB_Ne.setText("Ne");
        jB_Ne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_NeActionPerformed(evt);
            }
        });
        jPanel3.add(jB_Ne, java.awt.BorderLayout.CENTER);

        jB_Konec.setText("Konec hry");
        jB_Konec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_KonecActionPerformed(evt);
            }
        });
        jPanel3.add(jB_Konec, java.awt.BorderLayout.PAGE_END);

        jPanel2.add(jPanel3, java.awt.BorderLayout.LINE_END);

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_END);

        jB_A2.setBackground(new java.awt.Color(255, 255, 255));
        jB_A2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_A2.setBorderPainted(false);
        jB_A2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_A2MouseClicked(evt);
            }
        });
        jB_A2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_A2ActionPerformed(evt);
            }
        });

        jB_A4.setBackground(new java.awt.Color(255, 255, 255));
        jB_A4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_A4.setBorderPainted(false);
        jB_A4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_A4MouseClicked(evt);
            }
        });
        jB_A4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_A4ActionPerformed(evt);
            }
        });

        jB_A6.setBackground(new java.awt.Color(255, 255, 255));
        jB_A6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_A6.setBorderPainted(false);
        jB_A6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_A6MouseClicked(evt);
            }
        });
        jB_A6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_A6ActionPerformed(evt);
            }
        });

        jB_A8.setBackground(new java.awt.Color(255, 255, 255));
        jB_A8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_A8.setBorderPainted(false);
        jB_A8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_A8MouseClicked(evt);
            }
        });
        jB_A8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_A8ActionPerformed(evt);
            }
        });

        jB_B1.setBackground(new java.awt.Color(255, 255, 255));
        jB_B1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_B1.setBorderPainted(false);
        jB_B1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_B1MouseClicked(evt);
            }
        });
        jB_B1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_B1ActionPerformed(evt);
            }
        });

        jB_B3.setBackground(new java.awt.Color(255, 255, 255));
        jB_B3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_B3.setBorderPainted(false);
        jB_B3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_B3MouseClicked(evt);
            }
        });
        jB_B3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_B3ActionPerformed(evt);
            }
        });

        jB_B5.setBackground(new java.awt.Color(255, 255, 255));
        jB_B5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_B5.setBorderPainted(false);
        jB_B5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_B5MouseClicked(evt);
            }
        });
        jB_B5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_B5ActionPerformed(evt);
            }
        });

        jB_B7.setBackground(new java.awt.Color(255, 255, 255));
        jB_B7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_B7.setBorderPainted(false);
        jB_B7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_B7MouseClicked(evt);
            }
        });
        jB_B7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_B7ActionPerformed(evt);
            }
        });

        jB_C2.setBackground(new java.awt.Color(255, 255, 255));
        jB_C2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_C2.setBorderPainted(false);
        jB_C2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_C2MouseClicked(evt);
            }
        });
        jB_C2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_C2ActionPerformed(evt);
            }
        });

        jB_C4.setBackground(new java.awt.Color(255, 255, 255));
        jB_C4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_C4.setBorderPainted(false);
        jB_C4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_C4MouseClicked(evt);
            }
        });
        jB_C4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_C4ActionPerformed(evt);
            }
        });

        jB_C6.setBackground(new java.awt.Color(255, 255, 255));
        jB_C6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_C6.setBorderPainted(false);
        jB_C6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_C6MouseClicked(evt);
            }
        });
        jB_C6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_C6ActionPerformed(evt);
            }
        });

        jB_C8.setBackground(new java.awt.Color(255, 255, 255));
        jB_C8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_C8.setBorderPainted(false);
        jB_C8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_C8MouseClicked(evt);
            }
        });
        jB_C8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_C8ActionPerformed(evt);
            }
        });

        jB_D1.setBackground(new java.awt.Color(255, 255, 255));
        jB_D1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_D1.setBorderPainted(false);
        jB_D1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_D1MouseClicked(evt);
            }
        });
        jB_D1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_D1ActionPerformed(evt);
            }
        });

        jB_D3.setBackground(new java.awt.Color(255, 255, 255));
        jB_D3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_D3.setBorderPainted(false);
        jB_D3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_D3MouseClicked(evt);
            }
        });
        jB_D3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_D3ActionPerformed(evt);
            }
        });

        jB_D5.setBackground(new java.awt.Color(255, 255, 255));
        jB_D5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_D5.setBorderPainted(false);
        jB_D5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_D5MouseClicked(evt);
            }
        });
        jB_D5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_D5ActionPerformed(evt);
            }
        });

        jB_D7.setBackground(new java.awt.Color(255, 255, 255));
        jB_D7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_D7.setBorderPainted(false);
        jB_D7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_D7MouseClicked(evt);
            }
        });
        jB_D7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_D7ActionPerformed(evt);
            }
        });

        jB_E2.setBackground(new java.awt.Color(255, 255, 255));
        jB_E2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_E2.setBorderPainted(false);
        jB_E2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_E2MouseClicked(evt);
            }
        });
        jB_E2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_E2ActionPerformed(evt);
            }
        });

        jB_E4.setBackground(new java.awt.Color(255, 255, 255));
        jB_E4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_E4.setBorderPainted(false);
        jB_E4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_E4MouseClicked(evt);
            }
        });
        jB_E4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_E4ActionPerformed(evt);
            }
        });

        jB_E6.setBackground(new java.awt.Color(255, 255, 255));
        jB_E6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_E6.setBorderPainted(false);
        jB_E6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_E6MouseClicked(evt);
            }
        });
        jB_E6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_E6ActionPerformed(evt);
            }
        });

        jB_E8.setBackground(new java.awt.Color(255, 255, 255));
        jB_E8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_E8.setBorderPainted(false);
        jB_E8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_E8MouseClicked(evt);
            }
        });
        jB_E8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_E8ActionPerformed(evt);
            }
        });

        jB_F1.setBackground(new java.awt.Color(255, 255, 255));
        jB_F1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_F1.setBorderPainted(false);
        jB_F1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_F1MouseClicked(evt);
            }
        });
        jB_F1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_F1ActionPerformed(evt);
            }
        });

        jB_F3.setBackground(new java.awt.Color(255, 255, 255));
        jB_F3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_F3.setBorderPainted(false);
        jB_F3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_F3MouseClicked(evt);
            }
        });
        jB_F3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_F3ActionPerformed(evt);
            }
        });

        jB_F5.setBackground(new java.awt.Color(255, 255, 255));
        jB_F5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_F5.setBorderPainted(false);
        jB_F5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_F5MouseClicked(evt);
            }
        });
        jB_F5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_F5ActionPerformed(evt);
            }
        });

        jB_F7.setBackground(new java.awt.Color(255, 255, 255));
        jB_F7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_F7.setBorderPainted(false);
        jB_F7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_F7MouseClicked(evt);
            }
        });
        jB_F7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_F7ActionPerformed(evt);
            }
        });

        jB_G2.setBackground(new java.awt.Color(255, 255, 255));
        jB_G2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_G2.setBorderPainted(false);
        jB_G2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_G2MouseClicked(evt);
            }
        });
        jB_G2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_G2ActionPerformed(evt);
            }
        });

        jB_G4.setBackground(new java.awt.Color(255, 255, 255));
        jB_G4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_G4.setBorderPainted(false);
        jB_G4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_G4MouseClicked(evt);
            }
        });
        jB_G4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_G4ActionPerformed(evt);
            }
        });

        jB_G6.setBackground(new java.awt.Color(255, 255, 255));
        jB_G6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_G6.setBorderPainted(false);
        jB_G6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_G6MouseClicked(evt);
            }
        });
        jB_G6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_G6ActionPerformed(evt);
            }
        });

        jB_G8.setBackground(new java.awt.Color(255, 255, 255));
        jB_G8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_G8.setBorderPainted(false);
        jB_G8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_G8MouseClicked(evt);
            }
        });
        jB_G8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_G8ActionPerformed(evt);
            }
        });

        jB_H1.setBackground(new java.awt.Color(255, 255, 255));
        jB_H1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_H1.setBorderPainted(false);
        jB_H1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_H1MouseClicked(evt);
            }
        });
        jB_H1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_H1ActionPerformed(evt);
            }
        });

        jB_H3.setBackground(new java.awt.Color(255, 255, 255));
        jB_H3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_H3.setBorderPainted(false);
        jB_H3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_H3MouseClicked(evt);
            }
        });
        jB_H3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_H3ActionPerformed(evt);
            }
        });

        jB_H5.setBackground(new java.awt.Color(255, 255, 255));
        jB_H5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_H5.setBorderPainted(false);
        jB_H5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_H5MouseClicked(evt);
            }
        });
        jB_H5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_H5ActionPerformed(evt);
            }
        });

        jB_H7.setBackground(new java.awt.Color(255, 255, 255));
        jB_H7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jB_H7.setBorderPainted(false);
        jB_H7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_H7MouseClicked(evt);
            }
        });
        jB_H7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_H7ActionPerformed(evt);
            }
        });

        jButton1.setBackground(new java.awt.Color(0, 0, 0));
        jButton1.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton1.setEnabled(false);
        jButton1.setName(""); // NOI18N

        jButton2.setBackground(new java.awt.Color(0, 0, 0));
        jButton2.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton2.setEnabled(false);
        jButton2.setName(""); // NOI18N

        jButton5.setBackground(new java.awt.Color(0, 0, 0));
        jButton5.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton5.setEnabled(false);
        jButton5.setName(""); // NOI18N

        jButton7.setBackground(new java.awt.Color(0, 0, 0));
        jButton7.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton7.setEnabled(false);
        jButton7.setName(""); // NOI18N

        jButton10.setBackground(new java.awt.Color(0, 0, 0));
        jButton10.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton10.setEnabled(false);
        jButton10.setName(""); // NOI18N

        jButton12.setBackground(new java.awt.Color(0, 0, 0));
        jButton12.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton12.setEnabled(false);
        jButton12.setName(""); // NOI18N

        jButton14.setBackground(new java.awt.Color(0, 0, 0));
        jButton14.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton14.setEnabled(false);
        jButton14.setName(""); // NOI18N

        jButton16.setBackground(new java.awt.Color(0, 0, 0));
        jButton16.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton16.setEnabled(false);
        jButton16.setName(""); // NOI18N

        jButton17.setBackground(new java.awt.Color(0, 0, 0));
        jButton17.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton17.setEnabled(false);
        jButton17.setName(""); // NOI18N

        jButton19.setBackground(new java.awt.Color(0, 0, 0));
        jButton19.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton19.setEnabled(false);
        jButton19.setName(""); // NOI18N

        jButton21.setBackground(new java.awt.Color(0, 0, 0));
        jButton21.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton21.setEnabled(false);
        jButton21.setName(""); // NOI18N

        jButton23.setBackground(new java.awt.Color(0, 0, 0));
        jButton23.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton23.setEnabled(false);
        jButton23.setName(""); // NOI18N

        jButton26.setBackground(new java.awt.Color(0, 0, 0));
        jButton26.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton26.setEnabled(false);
        jButton26.setName(""); // NOI18N

        jButton28.setBackground(new java.awt.Color(0, 0, 0));
        jButton28.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton28.setEnabled(false);
        jButton28.setName(""); // NOI18N

        jButton30.setBackground(new java.awt.Color(0, 0, 0));
        jButton30.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton30.setEnabled(false);
        jButton30.setName(""); // NOI18N

        jButton32.setBackground(new java.awt.Color(0, 0, 0));
        jButton32.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton32.setEnabled(false);
        jButton32.setName(""); // NOI18N

        jButton33.setBackground(new java.awt.Color(0, 0, 0));
        jButton33.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton33.setEnabled(false);
        jButton33.setName(""); // NOI18N

        jButton35.setBackground(new java.awt.Color(0, 0, 0));
        jButton35.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton35.setEnabled(false);
        jButton35.setName(""); // NOI18N

        jButton37.setBackground(new java.awt.Color(0, 0, 0));
        jButton37.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton37.setEnabled(false);
        jButton37.setName(""); // NOI18N

        jButton39.setBackground(new java.awt.Color(0, 0, 0));
        jButton39.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton39.setEnabled(false);
        jButton39.setName(""); // NOI18N

        jButton41.setBackground(new java.awt.Color(0, 0, 0));
        jButton41.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton41.setEnabled(false);
        jButton41.setName(""); // NOI18N

        jButton43.setBackground(new java.awt.Color(0, 0, 0));
        jButton43.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton43.setEnabled(false);
        jButton43.setName(""); // NOI18N

        jButton45.setBackground(new java.awt.Color(0, 0, 0));
        jButton45.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton45.setEnabled(false);
        jButton45.setName(""); // NOI18N

        jButton47.setBackground(new java.awt.Color(0, 0, 0));
        jButton47.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton47.setEnabled(false);
        jButton47.setName(""); // NOI18N

        jButton50.setBackground(new java.awt.Color(0, 0, 0));
        jButton50.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton50.setEnabled(false);
        jButton50.setName(""); // NOI18N

        jButton52.setBackground(new java.awt.Color(0, 0, 0));
        jButton52.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton52.setEnabled(false);
        jButton52.setName(""); // NOI18N

        jButton54.setBackground(new java.awt.Color(0, 0, 0));
        jButton54.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton54.setEnabled(false);
        jButton54.setName(""); // NOI18N

        jButton56.setBackground(new java.awt.Color(0, 0, 0));
        jButton56.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton56.setEnabled(false);
        jButton56.setName(""); // NOI18N

        jButton27.setBackground(new java.awt.Color(0, 0, 0));
        jButton27.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton27.setEnabled(false);
        jButton27.setName(""); // NOI18N

        jButton24.setBackground(new java.awt.Color(0, 0, 0));
        jButton24.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton24.setEnabled(false);
        jButton24.setName(""); // NOI18N

        jButton20.setBackground(new java.awt.Color(0, 0, 0));
        jButton20.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton20.setEnabled(false);
        jButton20.setName(""); // NOI18N

        jButton31.setBackground(new java.awt.Color(0, 0, 0));
        jButton31.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        jButton31.setEnabled(false);
        jButton31.setName(""); // NOI18N

        label1.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label1.setText("7");

        label2.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label2.setText("8");

        label3.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label3.setText("5");

        label4.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label4.setText("6");

        label5.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label5.setText("3");

        label6.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label6.setText("1");

        label7.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label7.setText("2");

        label8.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label8.setText("4");

        label9.setAlignment(java.awt.Label.CENTER);
        label9.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label9.setText("A");

        label11.setAlignment(java.awt.Label.CENTER);
        label11.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label11.setText("C");

        label12.setAlignment(java.awt.Label.CENTER);
        label12.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label12.setText("B");

        label13.setAlignment(java.awt.Label.CENTER);
        label13.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label13.setText("D");

        label14.setAlignment(java.awt.Label.CENTER);
        label14.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label14.setText("E");

        label15.setAlignment(java.awt.Label.CENTER);
        label15.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label15.setText("F");

        label16.setAlignment(java.awt.Label.CENTER);
        label16.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label16.setText("G");

        label17.setAlignment(java.awt.Label.CENTER);
        label17.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        label17.setText("H");

        javax.swing.GroupLayout jPHraciPlochaLayout = new javax.swing.GroupLayout(jPHraciPlocha);
        jPHraciPlocha.setLayout(jPHraciPlochaLayout);
        jPHraciPlochaLayout.setHorizontalGroup(
            jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                            .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jB_A6, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButton17, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jB_B7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jB_B5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                    .addComponent(jB_C6, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jB_E6, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jB_D7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                    .addComponent(jButton19, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jB_D5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jB_F7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jB_F5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButton14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                    .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jB_H7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                    .addComponent(jButton23, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jB_H5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPHraciPlochaLayout.createSequentialGroup()
                                    .addComponent(jB_G6, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPHraciPlochaLayout.createSequentialGroup()
                            .addComponent(jB_A8, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButton20, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jB_C8, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButton24, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jB_E8, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButton27, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jB_G8, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButton31, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                        .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jB_A2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jB_A4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton33, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton41, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton50, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton26, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jB_B3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jB_B1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                .addComponent(label9, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label12, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                        .addComponent(jB_C2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton52, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jB_E2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                        .addComponent(jB_C4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton28, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jB_E4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                        .addComponent(jButton35, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jB_D3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton37, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                        .addComponent(jButton43, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jB_D1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton45, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton30, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jB_F3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jB_F1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton54, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                        .addComponent(jB_G4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton32, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                        .addComponent(jButton39, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jB_H3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                        .addComponent(jButton47, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jB_H1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPHraciPlochaLayout.createSequentialGroup()
                                        .addComponent(jB_G2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton56, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                                .addComponent(label11, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label13, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label14, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label15, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label16, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label17, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(21, 21, 21)
                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPHraciPlochaLayout.setVerticalGroup(
            jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton31, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                    .addComponent(jB_A8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                    .addComponent(jB_C8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                    .addComponent(jButton20, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                    .addComponent(jButton24, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                    .addComponent(jB_E8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                    .addComponent(jB_G8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                    .addComponent(jButton27, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                    .addComponent(label2, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jB_B7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jB_D7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jB_F7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jB_H7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jB_A6, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jB_C6, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jB_E6, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jB_G6, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                        .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton17, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton19, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_B5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_D5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton23, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_F5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_H5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jB_A4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_C4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton26, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton28, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_E4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_G4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton30, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton32, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton33, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton35, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_B3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_D3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton37, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton39, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_F3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_H3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                        .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label8, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                        .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jB_A2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_C2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton50, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton52, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_E2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jB_G2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton54, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton56, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jButton41, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButton43, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jB_B1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButton45, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButton47, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jB_F1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jB_H1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jB_D1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPHraciPlochaLayout.createSequentialGroup()
                        .addComponent(label7, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPHraciPlochaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label9, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label11, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label12, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label13, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label14, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label15, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label16, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label17, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton1.getAccessibleContext().setAccessibleDescription("");

        getContentPane().add(jPHraciPlocha, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    /**/
    private void jB_AnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_AnoActionPerformed
        // TODO add your handling code here:
        jB_Ne.setVisible(false);
        jB_Ano.setEnabled(false);
        jB_Ano.setVisible(false);
    }//GEN-LAST:event_jB_AnoActionPerformed

    private void jB_NeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_NeActionPerformed
        // TODO add your handling code here:
        jB_Ne.setEnabled(false);
        jB_Ne.setVisible(false);
        jB_Ano.setVisible(false);
    }//GEN-LAST:event_jB_NeActionPerformed
// <editor-fold defaultstate="collapsed" desc="Generated Code">   
    // Variables declaration - do not modify        
    private void jB_KonecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_KonecActionPerformed
        // TODO add your handling code here:
        jB_Konec.setEnabled(false);
    }//GEN-LAST:event_jB_KonecActionPerformed

    private void jB_H7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_H7ActionPerformed
        // TODO add your handling code here:
        jB_H7.setEnabled(false);
    }//GEN-LAST:event_jB_H7ActionPerformed

    private void jB_H7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_H7MouseClicked
        // TODO add your handling code here:
        //    jB_H7.setEnabled(false);
    }//GEN-LAST:event_jB_H7MouseClicked

    private void jB_H5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_H5ActionPerformed
        // TODO add your handling code here:
        jB_H5.setEnabled(false);
    }//GEN-LAST:event_jB_H5ActionPerformed

    private void jB_H5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_H5MouseClicked
        // TODO add your handling code here:
        //   jB_H5.setEnabled(false);
    }//GEN-LAST:event_jB_H5MouseClicked

    private void jB_H3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_H3ActionPerformed
        // TODO add your handling code here:
        jB_H3.setEnabled(false);
    }//GEN-LAST:event_jB_H3ActionPerformed

    private void jB_H3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_H3MouseClicked
        // TODO add your handling code here:
        //    jB_H3.setEnabled(false);
    }//GEN-LAST:event_jB_H3MouseClicked

    private void jB_H1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_H1ActionPerformed
        // TODO add your handling code here:
        jB_H1.setEnabled(false);
    }//GEN-LAST:event_jB_H1ActionPerformed

    private void jB_H1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_H1MouseClicked
        // TODO add your handling code here:
        //    jB_H1.setEnabled(false);
    }//GEN-LAST:event_jB_H1MouseClicked

    private void jB_G8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_G8ActionPerformed
        // TODO add your handling code here:
        jB_G8.setEnabled(false);
    }//GEN-LAST:event_jB_G8ActionPerformed

    private void jB_G8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_G8MouseClicked
        // TODO add your handling code here:
        //    jB_G8.setEnabled(false);
    }//GEN-LAST:event_jB_G8MouseClicked

    private void jB_G6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_G6ActionPerformed
        // TODO add your handling code here:
        jB_G6.setEnabled(false);
    }//GEN-LAST:event_jB_G6ActionPerformed

    private void jB_G6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_G6MouseClicked
        // TODO add your handling code here:
        //    jB_G6.setEnabled(false);
    }//GEN-LAST:event_jB_G6MouseClicked

    private void jB_G4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_G4ActionPerformed
        // TODO add your handling code here:
        jB_G4.setEnabled(false);
    }//GEN-LAST:event_jB_G4ActionPerformed

    private void jB_G4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_G4MouseClicked
        // TODO add your handling code here:
        //  jB_G4.setEnabled(false);
    }//GEN-LAST:event_jB_G4MouseClicked

    private void jB_G2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_G2ActionPerformed
        // TODO add your handling code here:
        jB_G2.setEnabled(false);
    }//GEN-LAST:event_jB_G2ActionPerformed

    private void jB_G2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_G2MouseClicked
        // TODO add your handling code here:
        //    jB_G2.setEnabled(false);
    }//GEN-LAST:event_jB_G2MouseClicked

    private void jB_F7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_F7ActionPerformed
        // TODO add your handling code here:
        jB_F7.setEnabled(false);
    }//GEN-LAST:event_jB_F7ActionPerformed

    private void jB_F7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_F7MouseClicked
        // TODO add your handling code here:
        //      jB_F7.setEnabled(false);
    }//GEN-LAST:event_jB_F7MouseClicked

    private void jB_F5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_F5ActionPerformed
        // TODO add your handling code here:
        jB_F5.setEnabled(false);
    }//GEN-LAST:event_jB_F5ActionPerformed

    private void jB_F5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_F5MouseClicked
        // TODO add your handling code here:
        //   jB_F5.setEnabled(false);
    }//GEN-LAST:event_jB_F5MouseClicked

    private void jB_F3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_F3ActionPerformed
        // TODO add your handling code here:
        jB_F3.setEnabled(false);
    }//GEN-LAST:event_jB_F3ActionPerformed

    private void jB_F3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_F3MouseClicked
        // TODO add your handling code here:
        //  jB_F3.setEnabled(false);
    }//GEN-LAST:event_jB_F3MouseClicked

    private void jB_F1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_F1ActionPerformed
        // TODO add your handling code here:
        jB_F1.setEnabled(false);
    }//GEN-LAST:event_jB_F1ActionPerformed

    private void jB_F1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_F1MouseClicked
        // TODO add your handling code here:
        //  jB_F1.setEnabled(false);
    }//GEN-LAST:event_jB_F1MouseClicked

    private void jB_E8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_E8ActionPerformed
        // TODO add your handling code here:
        jB_E8.setEnabled(false);
    }//GEN-LAST:event_jB_E8ActionPerformed

    private void jB_E8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_E8MouseClicked
        // TODO add your handling code here:
        //     jB_E8.setEnabled(false);
    }//GEN-LAST:event_jB_E8MouseClicked

    private void jB_E6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_E6ActionPerformed
        // TODO add your handling code here:
        jB_E6.setEnabled(false);
    }//GEN-LAST:event_jB_E6ActionPerformed

    private void jB_E6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_E6MouseClicked
        // TODO add your handling code here:
        //    jB_E6.setEnabled(false);
    }//GEN-LAST:event_jB_E6MouseClicked

    private void jB_E4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_E4ActionPerformed
        // TODO add your handling code here:
        jB_E4.setEnabled(false);
    }//GEN-LAST:event_jB_E4ActionPerformed

    private void jB_E4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_E4MouseClicked
        // TODO add your handling code here:
        //    jB_E4.setEnabled(false);
    }//GEN-LAST:event_jB_E4MouseClicked

    private void jB_E2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_E2ActionPerformed
        // TODO add your handling code here:
        jB_E2.setEnabled(false);
    }//GEN-LAST:event_jB_E2ActionPerformed

    private void jB_E2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_E2MouseClicked
        // TODO add your handling code here:
        //   jB_E2.setEnabled(false);
    }//GEN-LAST:event_jB_E2MouseClicked

    private void jB_D7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_D7ActionPerformed
        // TODO add your handling code here:
        jB_D7.setEnabled(false);
    }//GEN-LAST:event_jB_D7ActionPerformed

    private void jB_D7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_D7MouseClicked
        // TODO add your handling code here:
        //   jB_D7.setEnabled(false);
    }//GEN-LAST:event_jB_D7MouseClicked

    private void jB_D5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_D5ActionPerformed
        // TODO add your handling code here:
        jB_D5.setEnabled(false);
    }//GEN-LAST:event_jB_D5ActionPerformed

    private void jB_D5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_D5MouseClicked
        // TODO add your handling code here:
        //   jB_D5.setEnabled(false);
    }//GEN-LAST:event_jB_D5MouseClicked

    private void jB_D3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_D3ActionPerformed
        // TODO add your handling code here:
        jB_D3.setEnabled(false);
    }//GEN-LAST:event_jB_D3ActionPerformed

    private void jB_D3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_D3MouseClicked
        // TODO add your handling code here:
        //  jB_D3.setEnabled(false);
    }//GEN-LAST:event_jB_D3MouseClicked

    private void jB_D1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_D1ActionPerformed
        // TODO add your handling code here:
        jB_D1.setEnabled(false);
    }//GEN-LAST:event_jB_D1ActionPerformed

    private void jB_D1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_D1MouseClicked
        // TODO add your handling code here:
        //   jB_D1.setEnabled(false);
    }//GEN-LAST:event_jB_D1MouseClicked

    private void jB_C8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_C8ActionPerformed
        // TODO add your handling code here:
        jB_C8.setEnabled(false);
    }//GEN-LAST:event_jB_C8ActionPerformed

    private void jB_C8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_C8MouseClicked
        // TODO add your handling code here:
        //    jB_C8.setEnabled(false);
    }//GEN-LAST:event_jB_C8MouseClicked

    private void jB_C6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_C6ActionPerformed
        // TODO add your handling code here:
        jB_C6.setEnabled(false);
    }//GEN-LAST:event_jB_C6ActionPerformed

    private void jB_C6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_C6MouseClicked
        // TODO add your handling code here:
        //   jB_C6.setEnabled(false);
    }//GEN-LAST:event_jB_C6MouseClicked

    private void jB_C4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_C4ActionPerformed
        // TODO add your handling code here:
        jB_C4.setEnabled(false);
    }//GEN-LAST:event_jB_C4ActionPerformed

    private void jB_C4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_C4MouseClicked
        // TODO add your handling code here:
        //   jB_C4.setEnabled(false);
    }//GEN-LAST:event_jB_C4MouseClicked

    private void jB_C2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_C2ActionPerformed
        // TODO add your handling code here:
        jB_C2.setEnabled(false);
    }//GEN-LAST:event_jB_C2ActionPerformed

    private void jB_C2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_C2MouseClicked
        // TODO add your handling code here:
        //   jB_C2.setEnabled(false);
    }//GEN-LAST:event_jB_C2MouseClicked

    private void jB_B7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_B7ActionPerformed
        // TODO add your handling code here:
        jB_B7.setEnabled(false);
    }//GEN-LAST:event_jB_B7ActionPerformed

    private void jB_B7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_B7MouseClicked
        // TODO add your handling code here:
        //    jB_B7.setEnabled(false);
    }//GEN-LAST:event_jB_B7MouseClicked

    private void jB_B5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_B5ActionPerformed
        // TODO add your handling code here:
        jB_B5.setEnabled(false);
    }//GEN-LAST:event_jB_B5ActionPerformed

    private void jB_B5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_B5MouseClicked
        // TODO add your handling code here:
        //   jB_B5.setEnabled(false);
    }//GEN-LAST:event_jB_B5MouseClicked

    private void jB_B3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_B3ActionPerformed
        // TODO add your handling code here:
        jB_B3.setEnabled(false);
    }//GEN-LAST:event_jB_B3ActionPerformed

    private void jB_B3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_B3MouseClicked
        // TODO add your handling code here:
        //    jB_B3.setEnabled(false);
    }//GEN-LAST:event_jB_B3MouseClicked

    private void jB_B1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_B1ActionPerformed
        // TODO add your handling code here:
        jB_B1.setEnabled(false);
    }//GEN-LAST:event_jB_B1ActionPerformed

    private void jB_B1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_B1MouseClicked
        // TODO add your handling code here:
        //   jB_B1.setEnabled(false);
    }//GEN-LAST:event_jB_B1MouseClicked

    private void jB_A8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_A8ActionPerformed
        // TODO add your handling code here:
        jB_A8.setEnabled(false);
    }//GEN-LAST:event_jB_A8ActionPerformed

    private void jB_A8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_A8MouseClicked
        // TODO add your handling code here:
        //    jB_A8.setEnabled(false);
    }//GEN-LAST:event_jB_A8MouseClicked

    private void jB_A6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_A6ActionPerformed
        // TODO add your handling code here:
        jB_A6.setEnabled(false);
    }//GEN-LAST:event_jB_A6ActionPerformed

    private void jB_A6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_A6MouseClicked
        // TODO add your handling code here:
        //     jB_A6.setEnabled(false);
    }//GEN-LAST:event_jB_A6MouseClicked

    private void jB_A4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_A4ActionPerformed
        // TODO add your handling code here:
        jB_A4.setEnabled(false);
    }//GEN-LAST:event_jB_A4ActionPerformed

    private void jB_A4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_A4MouseClicked
        // TODO add your handling code here:
        //    jB_A4.setEnabled(false);
    }//GEN-LAST:event_jB_A4MouseClicked

    private void jB_A2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_A2ActionPerformed
        // TODO add your handling code here:
        jB_A2.setEnabled(false);
    }//GEN-LAST:event_jB_A2ActionPerformed

    private void jB_A2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_A2MouseClicked
        // TODO add your handling code here:
        //    jB_A2.setEnabled(false);
    }//GEN-LAST:event_jB_A2MouseClicked
    // End of variables declaration                   
// </editor-fold>   

    private void seActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seActionPerformed
        se.setEnabled(false);
        hrac = 0;
        runserver();

    }//GEN-LAST:event_seActionPerformed

    private void clActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clActionPerformed
        cl.setEnabled(false);
        hrac = 1;
        try {
            runclient();
        } catch (IOException ex) {
            Logger.getLogger(GUIDama.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_clActionPerformed

    private void sitovaHraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sitovaHraActionPerformed
        sitovaHra.setEnabled(false);
        sitovaHra.setVisible(false);
        mistniHra.setVisible(false);
    }//GEN-LAST:event_sitovaHraActionPerformed

    private void mistniHraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mistniHraActionPerformed
        mistniHra.setEnabled(false);
        sitovaHra.setVisible(false);
        mistniHra.setVisible(false);
        jB_Ne.setVisible(true);
        jB_Ano.setVisible(true);
    }//GEN-LAST:event_mistniHraActionPerformed

    private void jB_novaHraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_novaHraActionPerformed
        jB_novaHra.setVisible(false);
         anoNe = "";
                    sitovaHra.setEnabled(true);
                    mistniHra.setEnabled(true);
                    mistniHra.setVisible(true);
                    jB_Ne.setEnabled(true);
                    jB_Ano.setEnabled(true);
                    this.repaint();
                   
    }//GEN-LAST:event_jB_novaHraActionPerformed

    public void vlozObrazky() throws FileNotFoundException, IOException {

        ImageIcon BD = new ImageIcon(("BD.PNG"));
        ImageIcon CD = new ImageIcon(("CD.PNG"));
        ImageIcon BP = new ImageIcon(("BP.PNG"));
        ImageIcon CP = new ImageIcon(("CP.PNG"));
        ImageIcon N = new ImageIcon(("N.PNG"));
        ovlad.getFigurka();
        if (i == 0) {
            hraci = sachovnice.getHraci();
            i++;
        } else {
            vypisZesouboru();
            hraci = sachovnice.getHraci();
        }

        for (int a = 0; a < 8; a++) {
            for (int b = 0; b < 8; b++) {
                if (hraci[a][b] != null) {
                    if (hraci[a][b].barva == 0) {
                        if (hraci[a][b].druh == 0) {
                            obrazekTlacitek(a, b, BP);
                        } else if (hraci[a][b].druh == 1) {
                            obrazekTlacitek(a, b, BD);
                        }
                    } else if (hraci[a][b].barva == 1) {
                        if (hraci[a][b].druh == 0) {
                            obrazekTlacitek(a, b, CP);
                        } else if (hraci[a][b].druh == 1) {
                            obrazekTlacitek(a, b, CD);
                        }
                    }
                } else {
                    obrazekTlacitek(a, b, N);
                }
            }
        }

//
        //  jB_B1.setIcon(BD);

    }

    public void vypisZesouboru() throws FileNotFoundException, IOException {
        read = new BufferedReader(new FileReader("autosave.txt"));
        for (int j = 0; j < 24; j++) {
            radka = read.readLine();
            pole = radka.split(",");
            for (int k = 0; k < 4; k++) {
                {
                    figurky[j][k] = Integer.parseInt(pole[k]);
                }
            }
        }
        sachovnice = new HraciPlocha(figurky);
    }

    public int hraje() throws FileNotFoundException, IOException {
        int hraje;
        read = new BufferedReader(new FileReader("cislo.txt"));
        hraje = read.read();
        return hraje;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws FileNotFoundException, IOException, InterruptedException {

        new GUIDama();


    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cl;
    public javax.swing.JButton jB_A2;
    public javax.swing.JButton jB_A4;
    public javax.swing.JButton jB_A6;
    public javax.swing.JButton jB_A8;
    public javax.swing.JButton jB_Ano;
    public javax.swing.JButton jB_B1;
    public javax.swing.JButton jB_B3;
    public javax.swing.JButton jB_B5;
    public javax.swing.JButton jB_B7;
    public javax.swing.JButton jB_C2;
    public javax.swing.JButton jB_C4;
    public javax.swing.JButton jB_C6;
    public javax.swing.JButton jB_C8;
    public javax.swing.JButton jB_D1;
    public javax.swing.JButton jB_D3;
    public javax.swing.JButton jB_D5;
    public javax.swing.JButton jB_D7;
    public javax.swing.JButton jB_E2;
    public javax.swing.JButton jB_E4;
    public javax.swing.JButton jB_E6;
    public javax.swing.JButton jB_E8;
    public javax.swing.JButton jB_F1;
    public javax.swing.JButton jB_F3;
    public javax.swing.JButton jB_F5;
    public javax.swing.JButton jB_F7;
    public javax.swing.JButton jB_G2;
    public javax.swing.JButton jB_G4;
    public javax.swing.JButton jB_G6;
    public javax.swing.JButton jB_G8;
    public javax.swing.JButton jB_H1;
    public javax.swing.JButton jB_H3;
    public javax.swing.JButton jB_H5;
    public javax.swing.JButton jB_H7;
    private javax.swing.JButton jB_Konec;
    public javax.swing.JButton jB_Ne;
    private javax.swing.JButton jB_novaHra;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton26;
    private javax.swing.JButton jButton27;
    private javax.swing.JButton jButton28;
    private javax.swing.JButton jButton30;
    private javax.swing.JButton jButton31;
    private javax.swing.JButton jButton32;
    private javax.swing.JButton jButton33;
    private javax.swing.JButton jButton35;
    private javax.swing.JButton jButton37;
    private javax.swing.JButton jButton39;
    private javax.swing.JButton jButton41;
    private javax.swing.JButton jButton43;
    private javax.swing.JButton jButton45;
    private javax.swing.JButton jButton47;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton50;
    private javax.swing.JButton jButton52;
    private javax.swing.JButton jButton54;
    private javax.swing.JButton jButton56;
    private javax.swing.JButton jButton7;
    private javax.swing.JTextArea jL_T;
    private javax.swing.JPanel jPHraciPlocha;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.Label label1;
    private java.awt.Label label11;
    private java.awt.Label label12;
    private java.awt.Label label13;
    private java.awt.Label label14;
    private java.awt.Label label15;
    private java.awt.Label label16;
    private java.awt.Label label17;
    private java.awt.Label label2;
    private java.awt.Label label3;
    private java.awt.Label label4;
    private java.awt.Label label5;
    private java.awt.Label label6;
    private java.awt.Label label7;
    private java.awt.Label label8;
    private java.awt.Label label9;
    private javax.swing.JButton mistniHra;
    private javax.swing.JButton se;
    private javax.swing.JButton sitovaHra;
    // End of variables declaration//GEN-END:variables
// </editor-fold>   

    public void obrazekTlacitek(int x, int y, ImageIcon ico) {
        if (x == 0 && y == 1) {
            jB_B1.setIcon(ico);
        } else if (x == 0 && y == 3) {
            jB_D1.setIcon(ico);
        } else if (x == 0 && y == 5) {
            jB_F1.setIcon(ico);
        } else if (x == 0 && y == 7) {
            jB_H1.setIcon(ico);
        } else if (x == 1 && y == 0) {
            jB_A2.setIcon(ico);
        } else if (x == 1 && y == 2) {
            jB_C2.setIcon(ico);
        } else if (x == 1 && y == 4) {
            jB_E2.setIcon(ico);
        } else if (x == 1 && y == 6) {
            jB_G2.setIcon(ico);
        } else if (x == 2 && y == 1) {
            jB_B3.setIcon(ico);
        } else if (x == 2 && y == 3) {
            jB_D3.setIcon(ico);
        } else if (x == 2 && y == 5) {
            jB_F3.setIcon(ico);
        } else if (x == 2 && y == 7) {
            jB_H3.setIcon(ico);
        } else if (x == 3 && y == 0) {
            jB_A4.setIcon(ico);
        } else if (x == 3 && y == 2) {
            jB_C4.setIcon(ico);
        } else if (x == 3 && y == 4) {
            jB_E4.setIcon(ico);
        } else if (x == 3 && y == 6) {
            jB_G4.setIcon(ico);
        } else if (x == 4 && y == 1) {
            jB_B5.setIcon(ico);
        } else if (x == 4 && y == 3) {
            jB_D5.setIcon(ico);
        } else if (x == 4 && y == 5) {
            jB_F5.setIcon(ico);
        } else if (x == 4 && y == 7) {
            jB_H5.setIcon(ico);
        } else if (x == 5 && y == 0) {
            jB_A6.setIcon(ico);
        } else if (x == 5 && y == 2) {
            jB_C6.setIcon(ico);
        } else if (x == 5 && y == 4) {
            jB_E6.setIcon(ico);
        } else if (x == 5 && y == 6) {
            jB_G6.setIcon(ico);
        } else if (x == 6 && y == 1) {
            jB_B7.setIcon(ico);
        } else if (x == 6 && y == 3) {
            jB_D7.setIcon(ico);
        } else if (x == 6 && y == 5) {
            jB_F7.setIcon(ico);
        } else if (x == 6 && y == 7) {
            jB_H7.setIcon(ico);
        } else if (x == 7 && y == 0) {
            jB_A8.setIcon(ico);
        } else if (x == 7 && y == 2) {
            jB_C8.setIcon(ico);
        } else if (x == 7 && y == 4) {
            jB_E8.setIcon(ico);
        } else if (x == 7 && y == 6) {
            jB_G8.setIcon(ico);
        }
    }

    public void nastavEnableTrue() {

        jB_B1.setEnabled(true);
        jB_D1.setEnabled(true);
        jB_F1.setEnabled(true);
        jB_H1.setEnabled(true);
        jB_A2.setEnabled(true);
        jB_C2.setEnabled(true);
        jB_E2.setEnabled(true);
        jB_G2.setEnabled(true);
        jB_B3.setEnabled(true);
        jB_D3.setEnabled(true);
        jB_F3.setEnabled(true);
        jB_H3.setEnabled(true);
        jB_A4.setEnabled(true);
        jB_C4.setEnabled(true);
        jB_E4.setEnabled(true);
        jB_G4.setEnabled(true);
        jB_B5.setEnabled(true);
        jB_D5.setEnabled(true);
        jB_F5.setEnabled(true);
        jB_H5.setEnabled(true);
        jB_A6.setEnabled(true);
        jB_C6.setEnabled(true);
        jB_E6.setEnabled(true);
        jB_G6.setEnabled(true);
        jB_B7.setEnabled(true);
        jB_D7.setEnabled(true);
        jB_F7.setEnabled(true);
        jB_H7.setEnabled(true);
        jB_A8.setEnabled(true);
        jB_C8.setEnabled(true);
        jB_E8.setEnabled(true);
        jB_G8.setEnabled(true);
    }
}
