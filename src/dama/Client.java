/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dama;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author MrCvok
 */
public class Client {

    String inpName = "Autosave.txt";
    String poradi = "cislo.txt";
    String ip = "192.168.0.101";
    /*public void setInpName(String inpName) {
     this.inpName = inpName;
     }*/

    Client() {
        // defaultni port je 81
        int port = 12345;
        // defaultni input
        InputStream input = System.in;
        InputStream input2 = System.in;

        try {
            input2 = new java.io.FileInputStream(poradi);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        try {
            input = new java.io.FileInputStream(inpName);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        // vypis
        System.err.printf("Connecting to server, port %d...\n", port);
        try {
            //pokusi se pripojit na server na 127.0.0.1, na nastavenem portu
            Socket socket = new Socket(ip, port);
            //vypise ze posila data
            System.err.printf("Connected! Now I'll send him data from %s...\n", inpName);
            //vytvori printwriter pro posilani dat
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            //reader pro nacteni souboru s daty
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));

            String line, line2;
            //v cyklu odesle data
            while ((line = reader.readLine()) != null) {
                writer.println(line);
                writer.flush();
            }

            // konec
            System.err.printf("All data sent, exiting!\n");
            //uzavre reader a writer
            reader.close();
            writer.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (ConnectException e) {
            // chyba pri spojeni
            System.err.printf("Connection on port %d refused! Is the server running there?\n", port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Client(int hrac) throws IOException {
        // defaultni port je 81
        int port = 12346;
        // defaultni input
        InputStream input2 = System.in;

        FileWriter souborZapis = new FileWriter("cislo.txt", false);
        BufferedWriter br = new BufferedWriter(souborZapis);

        br.write(hrac);

        br.newLine();
        br.close();

        try {
            input2 = new java.io.FileInputStream(poradi);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        // vypis
        System.err.printf("Connecting to server, port %d...\n", port);
        try {
            //pokusi se pripojit na server na 127.0.0.1, na nastavenem portu
            Socket socket = new Socket(ip, port);
            //vypise ze posila data
            System.err.printf("Connected! Now I'll send him data from %s...\n", poradi);
            //vytvori printwriter pro posilani dat
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            //reader pro nacteni souboru s daty
            BufferedReader reader2 = new BufferedReader(new InputStreamReader(input2));

            String line, line2;
            //v cyklu odesle data
            while ((line2 = reader2.readLine()) != null) {
                writer.println(line2);
                writer.flush();
            }

            // konec
            System.err.printf("All data sent, exiting!\n");
            //uzavre reader a writer
            reader2.close();
            writer.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (ConnectException e) {
            // chyba pri spojeni
            System.err.printf("Connection on port %d refused! Is the server running there?\n", port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
