/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dama;

/**
 *
 * @author MrCvok
 */
public class FigurkaD {

    public int startX;
    public int startY;
    public int barva;//bila =1, cerna = 0
    public int druh;//pešak =0, dama = 1

    public FigurkaD() {
        this.startX = 0;
        this.startY = 0;
        this.barva = 3;
        this.druh = 0;
    }

    public FigurkaD(int startX, int startY,  int barva, int druh) {
        this.startX = startX;
        this.startY = startY;
        this.barva = barva;
        this.druh = druh;

    }
/*
 * Pravidla pro pohyb figurky
 * závisí na souřadnicích, prioritě
 */
    public boolean volnaCesta(HraciPlocha s, int sloupec, int radek, int priorita) {
        int rozdilR = Math.abs(radek - this.startX), rozdilS = Math.abs(sloupec - this.startY);
       
        if (this.druh == 0) {
            if (rozdilR == rozdilS /*|| rozdilR == 0 && rozdilS >= 2 || rozdilS == 0 && rozdilR >= 2 || rozdilR > 0 && rozdilS == 0/* */) {
                if (barva == 0 && startX > radek) {
                    if (s.prazdnePole(sloupec, radek) /*|| s.figurkaNaPoli(sloupec, radek).barva != this.barva*/) {
                        return true;
                    }
                } else if (barva == 1 && startX < radek) {
                     if (s.prazdnePole(sloupec, radek) /*|| s.figurkaNaPoli(sloupec, radek).barva != this.barva*/) {
                        return true;
                    }
                }
            }
        }
        if (this.druh == 1) {
            if (rozdilR == rozdilS) {
                if (s.prazdnePole(sloupec, radek)) {
                    return true;
                }
            }
        }
        return false;

    } 
    /*
     Převádí zadaný znak na číselnou hodnotu odpovídající souřadnici ŘÁDKU v poli šachovnice
     */

    public  int prevodSloupce(char s) {
        char[] povoleneSloupce = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
        int cislo = 9;
        for (int a = 0; a < 8; a++) {
            if (s == povoleneSloupce[a]) {
                cislo = a /*+ 1*/;
            }
        }
        return cislo;
    }
    /*
     Převádí zadaný znak na číselnou hodnotu odpovídající souřadnici SLOUPCE v poli šachovnice
     */

    public  int prevodRadku(char s) {
        char[] povoleneRadky = new char[]{'1', '2', '3', '4', '5', '6', '7', '8'};
        int cislo = 9;
        for (int a = 0; a < 8; a++) {
            if (s == povoleneRadky[a]) {
                cislo = a/* + 1*/;
            }
        }
        return cislo;
    }
    /*Převod číselnýchsouřadnic na vouřadnice v poli*/

    public  String prevodSloupceCh(int s) {
        String cislo = "";
        if (s == 0) {
            cislo = "a";
        } else if (s == 1) {
            cislo = "b";
        } else if (s == 2) {
            cislo = "c";
        } else if (s == 3) {
            cislo = "d";
        } else if (s == 4) {
            cislo = "e";
        } else if (s == 5) {
            cislo = "f";
        } else if (s == 6) {
            cislo = "g";
        } else if (s == 7) {
            cislo = "h";
        }

        return cislo;
    }

    public  String prevodRadkuCh(int s) {

        String cislo = "";
        if (s == 0) {
            cislo = "1";
        } else if (s == 1) {
            cislo = "2";
        } else if (s == 2) {
            cislo = "3";
        } else if (s == 3) {
            cislo = "4";
        } else if (s == 4) {
            cislo = "5";
        } else if (s == 5) {
            cislo = "6";
        } else if (s == 6) {
            cislo = "7";
        } else if (s == 7) {
            cislo = "8";
        }

        return cislo;
    }

    /*Zjištění, zda uvedený sloupec je použitelný*/
    public  boolean platnySloupec(char s) {
        char[] povoleneSloupce = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
        for (int a = 0; a < 8; a++) {
            if (s == povoleneSloupce[a]) {
                return true;
            }
        }
        return false;
    }/**/


    public  boolean platnyRadek(char s) {
        char[] povoleneRadky = new char[]{'1', '2', '3', '4', '5', '6', '7', '8'};
        for (int a = 0; a < 8; a++) {
            if (s == povoleneRadky[a]) {
                return true;
            }
        }
        return false;
    }
}
