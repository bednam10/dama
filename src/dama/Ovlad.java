/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dama;

import java.io.*;
import dama.HraciPlocha;
import java.net.ServerSocket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

/**
 *
 * @author MrCvok
 */
public class Ovlad {

    static BufferedReader read;
    static BufferedWriter autowriter;
    static String radka;
    static String[] pole, hrac = new String[]{"červený", "zelený"};
    public Scanner scan = new Scanner(System.in);// nacteni;
    static HraciPlocha sachovnice = new HraciPlocha();
    static FigurkaD figurka = new FigurkaD();
    static int radekC, sloupecC, i, priorita;
    static int radekV, sloupecV;
    static int[][] figurky = new int[24][4];
    public String nacteni;
    static int hracJ = 0, druh = 0;
    Calendar kalendar = Calendar.getInstance();
    // static int kolo = 0;
    // int[][][] XY = new int[2][3][3];
    static boolean pr1 = false, pr2 = false;
    static String nacteniPrvni = "", nacteniDruhy = "";
    static Test tes = new Test();
    static ArrayList<String> skok;
    static String[][] t = new String[4][2];
    static String vypis = "";
    DateFormat formatData = new SimpleDateFormat("d.MMMM yyyy H:mm");

    public void setRadekV(int radekV) {
        this.radekV = radekV;
    }

    public void setSloupecV(int sloupecV) {
        this.sloupecV = sloupecV;
    }

    public String getNacteni() {
        return nacteni;
    }

    public void setNacteni(String nacteni) {
        this.nacteni = nacteni;
    }

    public String getNacteniPrvni() {
        return nacteniPrvni;
    }

    public void setNacteniPrvni(String nacteniPrvni) {
        Ovlad.nacteniPrvni = nacteniPrvni;
    }

    public String getNacteniDruhy() {
        return nacteniDruhy;
    }

    public void setNacteniDruhy(String nacteniDruhy) {
        Ovlad.nacteniDruhy = nacteniDruhy;
    }

    public String getVypis() {
        return vypis;
    }

    public void setVypis(String vypis) {
        Ovlad.vypis = vypis;
    }

    void zakladniChodDamy() throws FileNotFoundException, IOException {

        int i = 0;
        startHry();
        int test = 0;
        do {
            vypisPole();
            if (!pr1) {
                if (!pr2) {
                    skok = proverOkoli(sachovnice.hrac);
                    boolean pokr = false;

                    prvniCast(pokr);
                }
            }
            if (!pr2) {

                druhaCast();
                i = 0;
                pr1 = pokracovani();
                getFigurka();
                saveHry();


            }
        } while (vyhra(pr2) == 0);

        vypisPoleKonce();


    }

    public boolean prvniCast(boolean pokr) throws IOException {
        boolean konec = false;
        do {
            pokr = false;
            setVypis("Zadejte výchozí pole:");
            System.out.println(getVypis());


            nacteniPrvni = scan.next().toLowerCase();/**/
            if (!pr2) {
                while (nacteniPrvni.length() != 2 || !figurka.platnySloupec(nacteniPrvni.charAt(0)) || !figurka.platnyRadek(nacteniPrvni.charAt(1))) {

                    System.out.println("Neplatné zadání\nZadejte výchozí pole:");
                    nacteniPrvni = scan.next().toLowerCase();
                    pr2 = predcasnyKonec(nacteniPrvni);
                }
                i = vlozeniPrvni(nacteniPrvni, i);
                konec = true;
            } else {
                break;
            }
        } while (i != 0);
        return konec;
    }

    public boolean druhaCast() throws IOException {

        do {
            System.out.println("Zadejte cílové pole:");

            nacteniDruhy = scan.next().toLowerCase();

            if (!pr2) {
                while (nacteniDruhy.length() != 2 || !figurka.platnySloupec(nacteniDruhy.charAt(0)) || !figurka.platnyRadek(nacteniDruhy.charAt(1))) {
                    System.out.println("Neplatné zadání\nZadejte cílové pole:");

                    nacteniDruhy = scan.next().toLowerCase();


                }
                i = vlozeniDruhe(nacteniDruhy, pr1);
            } else {
                i = 0;
            }
        } while (i != 0);

        return false;
    }

    /*
     * Star hry
     * načítání uložené pozice
     */
    public String stat() {
        return "Vítejte" + "\n" + "Chcete načíst uloženou hru?";
    }

    public void startHry() throws FileNotFoundException, IOException {
        System.out.println("Vítejte");
        System.out.println(formatData.format(kalendar.getTime()));
        System.out.println("Chcete načíst uloženou hru?");

    }

    public void startZaznamuHry() throws IOException {
        FileWriter souborZapis = new FileWriter("Záznam hry.txt", true);
        BufferedWriter br = new BufferedWriter(souborZapis);
        DateFormat formatData = new SimpleDateFormat("d.MMMM yyyy H:mm");
        br.write(formatData.format(kalendar.getTime()) + "\n");
        br.newLine();
        br.close();
    }

    public int nacteniHry(String nacteni) throws IOException {
        // nacteni = scan.next().toLowerCase();
        FileWriter souborZapis = new FileWriter("Záznam hry.txt", true);
        BufferedWriter br = new BufferedWriter(souborZapis);
        if (nacteni.equalsIgnoreCase("Ano") || nacteni.equalsIgnoreCase("A")) {
            System.out.println("Uložené pozice:");
            try {
                br.write("Načteno uloženou pozici.");
                read = new BufferedReader(new FileReader("autosave.txt"));
                for (int j = 0; j < 24; j++) {
                    radka = read.readLine();
                    pole = radka.split(",");
                    for (int k = 0; k < 4; k++) {
                        {
                            figurky[j][k] = Integer.parseInt(pole[k]);
                        }
                    }
                }
                sachovnice = new HraciPlocha(figurky);
                br.newLine();
                br.close();
            } catch (FileNotFoundException e) {

                System.out.println("--------------------\nŽádná uložená pozice.\n--------------------");
                System.out.println("Začínám novou hru");
                br.write("Nová hra");


                br.newLine();
                br.close();
            }

            return 1;
        } else {


            System.out.println("Začínám novou hru");
            br.write("Nová hra");
            br.newLine();
            br.close();

            return 0;
        }
    }

    /*
     * Zpracování prvních souřadnic
     */
    public int vlozeniPrvni(String nacteni, int pokracuj) throws IOException {

        /*prověřuje okolí figurky*/

        i = 0;
        if (pokracuj == 1 && nacteni != "") {
            setVypis("Zadejte povolené políčko.");
            System.out.println(getVypis());
            return 1;
        } else if (pokracuj == 1 && nacteni == "") {
            return 1;
        }
        this.nacteni = nacteni;
        sloupecV = figurka.prevodSloupce(nacteni.charAt(0));
        radekV = figurka.prevodRadku(nacteni.charAt(1));
        if (sachovnice.poleJePrazdne(sloupecV, radekV, 1)) {
            i = 2;
            setVypis("Pole " + nacteni.toUpperCase() + " je prázdné");

            System.out.println(getVypis());
            return i;
        } else {
            if (sachovnice.figurkaNaPoli(sloupecV, radekV).barva != sachovnice.hrac) {
                i = 2;
                setVypis("Na poli " + nacteni.toUpperCase() + " je soupeřova figurka");
                System.out.println(getVypis());
                return i;
            }
        }
        int rozdilR, rozdilS;
        int[][][] XY = new int[2][3][3];
        int i = 0, j = 0, m = 1;

        for (int rad = radekV - m; rad <= radekV + m; rad++) {
            for (int slo = sloupecV - m; slo <= sloupecV + m; slo++) {
                rozdilR = Math.abs(radekV - rad);
                rozdilS = Math.abs(sloupecV - slo);

                if (rozdilR == 1 && rozdilS == 1) {
                    XY[0][i][j] = rad;
                    XY[1][i][j] = slo;
                } else if (rozdilR == 0 && rozdilS == 1 || rozdilR == 1 && rozdilS == 0) {
                    XY[0][i][j] = 11;
                    XY[1][i][j] = 13;
                }

                if (j < 2) {
                    j++;
                } else {
                    j = 0;
                }
            }
            if (i < 2) {
                i++;
            } else {
                i = 0;
            }
        }

        vyberOkoli(1, XY);
        pozice(nacteni);

        return i;
    }

    public int vlozeniDruhe(String nacteni, boolean skok) throws IOException {

        i = 0;

        sloupecC = figurka.prevodSloupce(nacteni.charAt(0));
        radekC = figurka.prevodRadku(nacteni.charAt(1));
        if (radekC == radekV && sloupecC == sloupecV) {
            i = 1;
            setVypis("Nemá smysl přesouvat figurku na její současnou pozici");
            System.out.println(getVypis());
            return i;
        } else {

            if (!sachovnice.poleJePrazdne(sloupecC, radekC, 1)) {
                i = 2;
                setVypis("Na poli " + nacteni.toUpperCase() + " je figurka");
                System.out.println(getVypis());
                return i;

            } /**/ else if (sachovnice.figurkaNaPoli(sloupecV, radekV).barva != sachovnice.hrac) {
                i = 3;
                setVypis("Na poli " + nacteni.toUpperCase() + " je soupeřova figurka");
                System.out.println(getVypis());
                return i;

            } else if (!(sachovnice.figurkaNaPoli(sloupecV, radekV).volnaCesta(sachovnice, sloupecC, radekC, priorita)) && priorita != 1/* */) {
                i = 4;
                setVypis(("Na pole " + nacteni.toUpperCase() + " se nedá dostat"));
                System.out.println(getVypis());
                return i;

            } else if (skok && (sloupecC == (sloupecV + 1) || sloupecC == (sloupecV - 1))) {
                i = 6;
                setVypis(("Na pole " + nacteni.toUpperCase() + " se nedá dostat"));

                System.out.println(getVypis());
                return i;

            }
        }
        setVypis("");
        pozice(nacteni);
        return i;
    }

    /*
     * Vytvoření souboru s uložením hry
     */
    public void saveHry() throws IOException {
        autowriter = new BufferedWriter(new FileWriter("Autosave.txt"));
        for (int k = 0; k < 24; k++) {
            for (int l = 0; l < 4; l++) {
                autowriter.write(String.valueOf(figurky[k][l]) + ",");
            }
            autowriter.newLine();
        }
        autowriter.close();
    }

    public void vypisZaznam() throws FileNotFoundException, IOException {
          read = new BufferedReader(new FileReader("autosave.txt"));
            radka = read.readLine();
            pole = radka.split(",");
    }

    public void pozice(String zaznam) throws IOException {
        autowriter = new BufferedWriter(new FileWriter("a.txt", true));
        autowriter.write(zaznam+",");
        autowriter.close();
    }

    public void poziceVynul() throws IOException {
        autowriter = new BufferedWriter(new FileWriter("a.txt", false));
        autowriter.close();
    }
    
    public ArrayList<String> proverOkoli(int barva) throws IOException {
        /* for (int x = 0; x < 24; x++) {
         if (figurky[x][2] == barva) {*/

        String pom = null, pom2 = "";
        ArrayList<String> skok = new ArrayList<String>();
        // System.out.println("Skákejte ze souřadnice :"+pom);
        for (int a = 0; a < 8; a++) {
            for (int b = 0; b < 8; b++) {
                if (sachovnice.hraci[a][b] != null && hracJ == sachovnice.hraci[a][b].barva) {
                    druh = sachovnice.hraci[a][b].druh;
                    //  XY = sachovnice.okoli(sachovnice.hraci[a][b].startY, sachovnice.hraci[a][b].startX, hracJ);
                    int rozdilR, rozdilS;
                    int[][][] XY = new int[2][3][3];
                    int i = 0, j = 0, m = 1;

                    for (int rad = sachovnice.hraci[a][b].startX - m; rad <= sachovnice.hraci[a][b].startX + m; rad++) {
                        for (int slo = sachovnice.hraci[a][b].startY - m; slo <= sachovnice.hraci[a][b].startY + m; slo++) {
                            rozdilR = Math.abs(sachovnice.hraci[a][b].startX - rad);
                            rozdilS = Math.abs(sachovnice.hraci[a][b].startY - slo);

                            if (rozdilR == 1 && rozdilS == 1) {
                                XY[0][i][j] = rad;
                                XY[1][i][j] = slo;
                            } else if (rozdilR == 0 && rozdilS == 1 || rozdilR == 1 && rozdilS == 0) {
                                XY[0][i][j] = 11;
                                XY[1][i][j] = 13;
                            }

                            if (j < 2) {
                                j++;
                            } else {
                                j = 0;
                            }
                        }
                        if (i < 2) {
                            i++;
                        } else {
                            i = 0;
                        }
                    }
                    vyberOkoli(2, XY);
                    boolean p1 = false, p2 = false, p3 = false, p4 = false;
                    if ((druh == 1) || (druh == 0 && hracJ == 1)) {
                        try {
                            p1 = (sachovnice.hraci[a + 1][b + 1] != null) && (sachovnice.hraci[a + 2][b + 2] == null) && hracJ != sachovnice.hraci[a + 1][b + 1].barva;
                        } catch (Exception e) {

                            p1 = false;
                        }
                        try {
                            p2 = (sachovnice.hraci[a + 1][b - 1] != null) && (sachovnice.hraci[a + 2][b - 2] == null) && hracJ != sachovnice.hraci[a + 1][b - 1].barva;
                        } catch (Exception e) {

                            p2 = false;
                        }
                    }
                    if ((druh == 1) || (druh == 0 && hracJ == 0)) {
                        try {
                            p3 = (sachovnice.hraci[a - 1][b + 1] != null) && (sachovnice.hraci[a - 2][b + 2] == null) && hracJ != sachovnice.hraci[a - 1][b + 1].barva;
                        } catch (Exception e) {

                            p3 = false;
                        }
                        try {
                            p4 = (sachovnice.hraci[a - 1][b - 1] != null) && (sachovnice.hraci[a - 2][b - 2] == null) && hracJ != sachovnice.hraci[a - 1][b - 1].barva;
                        } catch (Exception e) {

                            p4 = false;
                        }
                    }
                    if (priorita == 1 && (p1 || p2 || p3 || p4)/*&& hracJ != sachovnice.hraci[a][b].barva*/) {
                        setVypis("Skákejte ze souřadnice :" + figurka.prevodSloupceCh(b) + figurka.prevodRadkuCh(a));
                        System.out.println(getVypis());
                        pom = "" + figurka.prevodSloupceCh(b) + figurka.prevodRadkuCh(a);
                        skok.add(pom);
                        pom2 += "Skákejte ze souřadnice :" + pom + "\n";
                    }
                    priorita = 0;

                    //  }}
                }
            }
        }
        setVypis(pom2);
        return skok;
    }


    /*   public static void kontrola(String s) throws IOException {

     if (nacteni.equals("exit")) {
     System.exit(0);
     }
     if (nacteni.equals("vypis")) {
     System.out.println(sachovnice.toString());
     System.out.println(s);
     nacteni = scan.next().toLowerCase();
     kontrola(s);
     }
     }*/
    /*
     *Zjištění, zda se má přeskakovat nebo ne
     */
    public void vyberOkoli(int p, int[][][] XY) throws IOException {

        int h = sachovnice.hrac;
        boolean i1 = sachovnice.poleJePrazdne(XY[1][2][0], XY[0][2][0], p),
                i2 = sachovnice.poleJePrazdne(XY[1][2][2], XY[0][2][2], p),
                i3 = sachovnice.poleJePrazdne(XY[1][0][2], XY[0][0][2], p),
                i4 = sachovnice.poleJePrazdne(XY[1][0][0], XY[0][0][0], p),
                a1 = sachovnice.barvaHrace(XY[1][2][0], XY[0][2][0], sachovnice.hrac),
                a2 = sachovnice.barvaHrace(XY[1][2][2], XY[0][2][2], sachovnice.hrac),
                a3 = sachovnice.barvaHrace(XY[1][0][2], XY[0][0][2], sachovnice.hrac),
                a4 = sachovnice.barvaHrace(XY[1][0][0], XY[0][0][0], sachovnice.hrac);

        if (h == 1 && (figurka.druh == 1 || druh == 1/**/)) {
            if ((i1) || (i2) || (i3) || (i4)) {
                priorita = 1;
            }
        } else if (h == 0 && (figurka.druh == /*0*/ 1 || druh == 1/**/)) {
            if ((i1) || (i2) || (i3) || (i4)) {
                priorita = 1;
            }
        } else if (h == 1 && (figurka.druh == 0 || druh == 0/**/)) {
            if ((!(i1) && !a1) || (!(i2) && !a2)) {
                priorita = 1;
            }
        } else if (h == 0 && (figurka.druh == 0/*1*/ || druh == 0/**/)) {
            if ((!(i3) && !a3) || (!(i4) && !a4)) {
                priorita = 1;
            }
        }

    }

    /*
     * Určuje, zda pokračuje stejný dráč, nebo druhý
     */
    public boolean pokracovani() throws IOException {
        boolean p = false;

        if (/*priorita == 1&&*/sachovnice.presunFigurku(sachovnice.figurkaNaPoli(sloupecV, radekV), sloupecC, radekC, priorita)) {

            sachovnice.hrac = sachovnice.hrac;
            hracJ = hracJ;
            p = true;
            priorita = 1;

        } else if (sachovnice.hrac == 0) {
            sachovnice.setHrac(1);
            hracJ = 1;
            p = false;
            priorita = 0;
        } else if (sachovnice.hrac == 1) {

            sachovnice.setHrac(0);
            hracJ = 0;
            p = false;
            priorita = 0;
        }
        return p;
    }

    public void getFigurka() {
        int[][] vypis = new int[24][4];

        int i = 0;
        for (int a = 0; a < 8; a++) {
            for (int b = 0; b < 8; b++) {
                if (sachovnice.hraci[a][b] != null) {
                    vypis[i][0] = sachovnice.hraci[a][b].startX;
                    vypis[i][1] = sachovnice.hraci[a][b].startY;
                    vypis[i][2] = sachovnice.hraci[a][b].barva;
                    vypis[i][3] = sachovnice.hraci[a][b].druh;
                    i++;
                }

            }
        }
        for (int j = 0; j < 24; j++) {
            if (vypis[j][0] == 0 && vypis[j][1] == 0) {
                vypis[j][2] = 3;
            }
        }
        figurky = vypis;

    }

    /*
     metoda k vypsání pole při začátku nového tahu
     */
    public void vypisPole() {

        String hraje = hrac[sachovnice.hrac];
        System.out.println(sachovnice.toString());
        System.out.println("Hraje " + hraje);
    }

    /*Předčasný konec*/
    public boolean predcasnyKonec(String nacteni) {

        if (nacteni.equalsIgnoreCase("konec")) {
            return true;
        } else {
            return false;
        }
    }

    /*
     metoda k vypsání pole při konci hry
     */
    public void vypisPoleKonce() {

        String hraje = hrac[sachovnice.hrac];
        System.out.println(sachovnice.toString());
    }

    /*
     určení vítěze
     * vrací číselnou hodnotu pro ukončení do-while cyklu, ve kterém program běží
     */
    public int vyhra(boolean predcasnyKonec) {
        //bila = 0, cerna = 1 
        int konec = 0; // 0=pokračuj, 1=černá, 2=bílá, 3=remíza
        String kon = "\nKonec hry\n---------------\n";
        int cerne = 0, bile = 0;
        for (int i = 0; i < 24; i++) {
            if (figurky[i][0] == 0 && figurky[i][1] == 0) {
            } else {
                if (figurky[i][2] == 1) {
                    cerne++;
                } else if (figurky[i][2] == 0/*&&(figurky[i][0] != 0&&figurky[i][1] != 0)*/) {
                    bile++;
                }
            }
        }
        if (!predcasnyKonec) {
            if (cerne == 1 && bile == 1) {
                //  System.out.println(kon + "Remíza");

                setVypis(kon + "Remíza");
                System.out.println(getVypis());
                konec = 1;
            } else if (bile <= 1) {
                // System.out.println(kon + "Vítězem je černý");
                setVypis(kon + "Vítězem je černý");
                System.out.println(getVypis());
                konec = 1;
            } else if (cerne <= 1) {
                //  System.out.println(kon + "Vítězem je bílý");
                setVypis(kon + "Vítězem je bílý");
                System.out.println(getVypis());
                konec = 1;
            } else {
                konec = 0;
            }
        } else if (predcasnyKonec) {
            if (cerne == bile) {
                setVypis(kon + "Remíza");
                System.out.println(getVypis());
                konec = 1;

            } else if (bile < cerne) {
                setVypis(kon + "Vítězem je černý");
                System.out.println(getVypis());
                konec = 1;
            } else if (bile > cerne) {
                setVypis(kon + "Vítězem je bílý");
                System.out.println(getVypis());
                konec = 1;
            }
        }

        return konec;

    }
    /*Vypsání záznamu hry do txt souboru*/

    public void zaznamHry(int barva, int sloupecS, int radekS, int sloupec, int radek) throws IOException {
        //cerna = 1, bila = 0
        String hrac = "";
        if (barva == 1) {
            hrac = "Černý";
        } else if (barva == 0) {
            hrac = "Býlý ";
        }
        FileWriter souborZapis = new FileWriter("Záznam hry.txt", true);
        BufferedWriter br = new BufferedWriter(souborZapis);

        br.write(hrac + "->Z pozice: radek " + (radekS + 1) + ", sloupec " + (sloupecS + 1) + "-> Na pozici: radek " + (radek + 1) + ", sloupec " + (sloupec + 1) + " ;");

        br.newLine();
        br.close();
    }

    public void zaznamHrySkok(int barva, int sloupecS, int radekS, int sloupec, int radek, int sloupecSkok, int radekSkok) throws IOException {

        String hrac = "";
        if (barva == 1) {
            hrac = "Černý";
        } else if (barva == 0) {
            hrac = "Býlý ";
        }
        FileWriter souborZapis = new FileWriter("Záznam hry.txt", true);
        BufferedWriter br = new BufferedWriter(souborZapis);

        br.write(hrac + "->Z pozice: radek " + (radekS + 1) + ", sloupec " + (sloupecS + 1) + "-> Na pozici: radek " + (radek + 1) + ", sloupec " + (sloupec + 1) + " ; Odstraněno na pozici: radek " + (radekSkok + 1) + ", sloupec " + (sloupecSkok + 1) + " ;");

        br.newLine();
        br.close();
    }
}
