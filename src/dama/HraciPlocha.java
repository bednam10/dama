/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dama;

import dama.FigurkaD;
import dama.Ovlad;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.ImageIcon;

/**
 *
 * @author MrCvok
 */
public class HraciPlocha {

    public FigurkaD[][] hraci = new FigurkaD[8][8];
    public int hrac;
    private Ovlad ovlad = new Ovlad();
    /*
     * Vytvoření figurek na hrací ploše
     */

    public HraciPlocha() {
        //cerna = 1, bila = 0


        this.hrac = 0;
        FigurkaD c01 = new FigurkaD(5, 0, /*1/*/ 0/**/, 0);
        FigurkaD c04 = new FigurkaD(5, 2, /*1/*/ 0/**/, 0);
        FigurkaD c07 = new FigurkaD(5, 4,/*1/*/ 0/**/, 0);
        FigurkaD c10 = new FigurkaD(5, 6, 0/*/ 1/**/, 0);

        FigurkaD c03 = new FigurkaD(6, 1, 0, 0);
        FigurkaD c06 = new FigurkaD(6, 3, /* 1/*/ 0/**/, /*1/*/ 0/**/);
        FigurkaD c09 = new FigurkaD(6, 5, 0, 0);
        FigurkaD c12 = new FigurkaD(6, 7, 0, 0);

        FigurkaD c02 = new FigurkaD(7, 0, 0, 0);
        FigurkaD c05 = new FigurkaD(7, 2, 0/*/ 1/**/, 0/*/ 1/**/);
        FigurkaD c08 = new FigurkaD(7, 4, 0, 0);
        FigurkaD c11 = new FigurkaD(7, 6, 0, 0);


        FigurkaD b02 = new FigurkaD(0, 1, /*0/*/ 1/**/, 0/*/ 1/**/);
        FigurkaD b05 = new FigurkaD(0, 3,/* 0/*/ 1/**/, 0/*/ 1/**/);
        FigurkaD b08 = new FigurkaD(0, 5, 1, 0);
        FigurkaD b11 = new FigurkaD(0, 7, 1, 0);

        FigurkaD b01 = new FigurkaD(1, 0, 1, 0);
        FigurkaD b04 = new FigurkaD(1, 2, 1, 0);
        FigurkaD b07 = new FigurkaD(1, 4, 1, 0);
        FigurkaD b10 = new FigurkaD(1, 6, 1, 0);

        FigurkaD b03 = new FigurkaD(2, 1, 1, 0);
        FigurkaD b06 = new FigurkaD(2, 3, 1, 0);
        FigurkaD b09 = new FigurkaD(2, 5, 1, 0);
        FigurkaD b12 = new FigurkaD(2, 7, 1, 0);

        FigurkaD p = new FigurkaD(/*3, 3,*/3, 0, 1, 0);
        FigurkaD p2 = new FigurkaD(4, 5, 0, 0/*/ 1/**/);

        FigurkaD[] poleFigurek = new FigurkaD[]{c01, c02, c03, c04, c05, c06, c07, c08, c09, c10, c11, c12, b01, b02, b03, b04, b05, b06, b07, b08, b09, b10, b11, b12 /*,p , p2*/};
        this.hraci = new FigurkaD[8][8];
        for (int i = 0; i < poleFigurek.length; i++) {
            hraci[poleFigurek[i].startX][poleFigurek[i].startY /*-1*/] = poleFigurek[i];


        }
    }
    /*Vypsání hodnot z pole hodnot do šachovnice*/

    public HraciPlocha(int[][] poleHodnot) {

        this.hraci = new FigurkaD[8][8];

        for (int i = 0; i < 24; i++) {
            if (poleHodnot[i][0] == 0 && poleHodnot[i][1] == 0) {
            } else {
                this.hraci[poleHodnot[i][0]][poleHodnot[i][1]] = new FigurkaD(poleHodnot[i][0], poleHodnot[i][1], poleHodnot[i][2], poleHodnot[i][3]);
            }
        }
    }

    public FigurkaD[][] getHraci() {
        return hraci;
    }

    public int getHrac() {
        return hrac;
    }

    public void setHrac(int hrac) {
        this.hrac = hrac;
    }

    /*
     * Zjištění, zda na zadaném poli je místo
     */
    public FigurkaD figurkaNaPoli(int sloupec, int radek) {
        return this.hraci[radek /*- 1*/][sloupec /*- 1*/];
    }

    /*
     * Zjištění, zda na zadaném poli je místo
     */
    public boolean prazdnePole(int sloupec, int radek) {
        try {
            return this.hraci[radek][sloupec] == null;
        } catch (Exception e) {
            return true;
        }
    }

    /*
     Ověření barvy hráče na poli
     */
    public boolean barvaHrace(int sloupec, int radek, int hrac) {
        try {
            return this.hraci[radek][sloupec].barva == hrac;
        } catch (Exception e) {
            return true;
        }
    }

    /*zjištění pro dánu, zda na požadované cestě neni nějaká překážka*/
    public boolean volnaCestaDamy(FigurkaD f, int sloupec, int radek) {
        boolean pomoc = false;
        int x = f.startX;
        int y = f.startY;
        if (f.startX < radek) {
            if (f.startY < sloupec) {
                while (x != radek && y != sloupec) {
                    x = x + 1;
                    y = y + 1;
                    pomoc = prazdnePole(y, x);
                    if (pomoc == false) {
                        return false;
                    }
                }
            } else if (f.startY > sloupec) {
                while (x != radek && y != sloupec) {
                    x = x + 1;
                    y = y - 1;
                    pomoc = prazdnePole(y, x);
                    if (pomoc == false) {
                        return false;
                    }
                }
            }
        } else if (f.startX > radek) {
            if (f.startY < sloupec) {
                while (x != radek && y != sloupec) {
                    x = x - 1;
                    y = y + 1;
                    pomoc = prazdnePole(y, x);
                    if (pomoc == false) {
                        return false;
                    }
                }
            } else if (f.startY > sloupec) {
                while (x != radek && y != sloupec) {
                    x = x - 1;
                    y = y - 1;
                    pomoc = prazdnePole(y, x);
                    if (pomoc == false) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /*
     * Přesouvání figurky
     * -> závisí na typu figurky
     * -> závisí na druhu skoku, zda se jedná o přesun, nebo skok
     */
    public boolean presunFigurku(FigurkaD f, int sloupec, int radek, int priorita/**/) throws IOException {

        boolean pokracujSkok0 = false, pokracujSkok1 = false, dlouhySkok = volnaCestaDamy(f, sloupec, radek);
        if (f.druh == 0) {


            this.hraci[radek/*-1*/][sloupec/*-1*/] = new FigurkaD(f.startX, f.startY, f.barva, f.druh);
            if (f.barva == 0 && priorita == 1) {
                preskocPesakBila(f, sloupec, radek, dlouhySkok);

            } else if (f.barva == 1 && priorita == 1) {
                prskocPesakCerna(f, sloupec, radek, dlouhySkok);

            } else {

                presunPesaka(f, sloupec, radek, dlouhySkok);
                return false;
            }
            povisPesaka(f, sloupec, radek, dlouhySkok);

            //cerna = 1, bila = 0
            if (priorita == 1) {
                try {
                    if ((((hraci[radek - 1][sloupec + 1] != null) && (hraci[radek - 2][sloupec + 2] == null))
                            || ((hraci[radek - 1][sloupec - 1] != null) && ((hraci[radek - 2][sloupec - 2] == null))))
                            && (f.barva == 0/* && (hraci[radek - 1][sloupec - 1].barva == 1 || hraci[radek - 1][sloupec + 1].barva == 1)*/)) {
                        if (hraci[radek - 1][sloupec + 1] != null) {
                            if (hraci[radek - 1][sloupec + 1].barva == 1) {
                                pokracujSkok0 = true;

                            }
                        }
                        if (hraci[radek - 1][sloupec - 1] != null) {
                            if (hraci[radek - 1][sloupec - 1].barva == 1) {
                                pokracujSkok0 = true;

                            }
                        }
                    } else if ((((hraci[radek + 1][sloupec + 1] != null) && (hraci[radek + 2][sloupec + 2] == null))
                            || ((hraci[radek + 1][sloupec - 1] != null) && ((hraci[radek + 2][sloupec - 2] == null))))
                            && (f.barva == 1/* && (hraci[radek + 1][sloupec - 1].barva == 0 || hraci[radek + 1][sloupec + 1].barva == 0)*/)) {

                        if (hraci[radek + 1][sloupec + 1] != null) {
                            if (hraci[radek + 1][sloupec + 1].barva == 0) {
                                pokracujSkok0 = true;

                            }
                        }
                        if (hraci[radek + 1][sloupec - 1] != null) {
                            if (hraci[radek + 1][sloupec - 1].barva == 0) {
                                pokracujSkok0 = true;

                            }
                        }
                    }

                    Ovlad.radekV = radek;
                    Ovlad.sloupecV = sloupec;
                } catch (Exception e) {
                    pokracujSkok0 = false;
                }
            }

        } else if (f.druh == 1) {
            int x = f.startX, y = f.startY;
            this.hraci[radek/*-1*/][sloupec/*-1*/] = new FigurkaD(f.startX, f.startY, f.barva, f.druh);

            if (f.startX < radek) {

                damaZadaneMensi(f, sloupec, radek, dlouhySkok);
            } else if (radek < f.startX) {
                damaZadaneVetsi(f, sloupec, radek, dlouhySkok);

            }
            Ovlad.radekV = x;
            Ovlad.sloupecV = y;
            try {

                if (((hraci[x - 1][y + 1] != null) && (hraci[x - 2][y + 2] == null))
                        || ((hraci[x - 1][y - 1] != null) && ((hraci[x - 2][y - 2] == null)))) {
                    pokracujSkok0 = true;
                }
                if (((hraci[x - 1][y + 1] != null) && (hraci[x - 2][y + 2] == null) && (hraci[x - 1][y + 1].barva != hrac))
                        || ((hraci[x - 1][y - 1] != null) && (hraci[x - 2][y - 2] == null) && (hraci[x - 1][y - 1].barva != hrac))
                        || ((hraci[x + 1][y + 1] != null) && (hraci[x + 2][y + 2] == null) && (hraci[x + 1][y + 1].barva != hrac))
                        || ((hraci[x + 1][y - 1] != null) && (hraci[x + 2][y - 2] == null) && (hraci[x + 1][y - 1].barva != hrac))) {
                    pokracujSkok0 = true;
                }
            } catch (Exception e) {

                pokracujSkok0 = false;
            }
        }
        if (pokracujSkok0) {
            return true;

        } else {
            return false;
        }
    }

    public void povisPesaka(FigurkaD f, int sloupec, int radek, boolean dlouhySkok) {
        if (f.barva == 1 && radek == 7 && (sloupec == 0 || sloupec == 2 || sloupec == 4 || sloupec == 6)) {
            this.hraci[radek][sloupec] = new FigurkaD(radek, sloupec, f.barva, 1);
        }
        if (f.barva == 0 && radek == 0 && (sloupec == 1 || sloupec == 3 || sloupec == 5 || sloupec == 7)) {
            this.hraci[radek][sloupec] = new FigurkaD(radek, sloupec, f.barva, 1);
        }
    }

    public void presunPesaka(FigurkaD f, int sloupec, int radek, boolean dlouhySkok) throws IOException {
        int x = f.startX, y = f.startY;
        this.hraci[f.startX][f.startY] = null;
        this.figurkaNaPoli(sloupec, radek).startX = radek;
        this.figurkaNaPoli(sloupec, radek).startY = sloupec;

        ovlad.zaznamHry(f.barva, f.startY, f.startX, sloupec, radek);
        povisPesaka(f, sloupec, radek, dlouhySkok);
    }

    public void preskocPesakBila(FigurkaD f, int sloupec, int radek, boolean dlouhySkok) throws IOException {
        int x = f.startX, y = f.startY;
        if (f.startY == sloupec - 1 || f.startY - 1 == sloupec) {
        } else if (/*f.startX > radek-1 && kolo ==0*/(f.startY < sloupec)) {

            this.hraci[ f.startX - 1][f.startY + 1] = null;

            ovlad.zaznamHrySkok(f.barva, f.startY, f.startX, sloupec, radek, f.startY + 1, f.startX - 1);

        } else if (f.startY > sloupec) {
            this.hraci[ f.startX - 1][f.startY - 1] = null;

            ovlad.zaznamHrySkok(f.barva, f.startY, f.startX, sloupec, radek, f.startY - 1, f.startX - 1);

        }
        this.hraci[f.startX/*-2*/][f.startY] = null;
        this.figurkaNaPoli(sloupec, radek).startX = radek;
        this.figurkaNaPoli(sloupec, radek).startY = sloupec;
    }

    public void prskocPesakCerna(FigurkaD f, int sloupec, int radek, boolean dlouhySkok) throws IOException {
        int x = f.startX, y = f.startY;

        if (f.startY == sloupec - 1 || f.startY - 1 == sloupec) {
        } else if (f.startY > sloupec) {
            this.hraci[f.startX + 1][f.startY - 1] = null;

            ovlad.zaznamHrySkok(f.barva, f.startY, f.startX, sloupec, radek, f.startY - 1, f.startX + 1);

        } else if (f.startY < sloupec) {
            this.hraci[f.startX + 1][f.startY + 1] = null;

            ovlad.zaznamHrySkok(f.barva, f.startY, f.startX, sloupec, radek, f.startY + 1, f.startX + 1);

        }
        this.hraci[ f.startX][f.startY] = null;
        this.figurkaNaPoli(sloupec, radek).startX = radek;
        this.figurkaNaPoli(sloupec, radek).startY = sloupec;

    }

    public void damaZadaneMensi(FigurkaD f, int sloupec, int radek, boolean dlouhySkok) throws IOException {
        int x = f.startX, y = f.startY;
        if ((f.startY + 1 == sloupec || f.startY - 1 == sloupec) || dlouhySkok) {
            this.hraci[f.startX][f.startY] = null;
            this.figurkaNaPoli(sloupec, radek).startX = radek;
            this.figurkaNaPoli(sloupec, radek).startY = sloupec;

            ovlad.zaznamHry(f.barva, f.startY, f.startX, sloupec, radek);
        } else if (f.startY < sloupec) {
            while (hraci[x + 1][y + 1] == null) {
                x++;
                y++;
            }
            x++;
            y++;
            this.hraci[ radek][sloupec] = null;

            this.hraci[ x][y] = null;
            this.hraci[f.startX][f.startY] = null;
            x = x + 1;
            y = y + 1;
            this.hraci[x/*-1*/][y/*-1*/] = new FigurkaD(x, y, f.barva, f.druh);

            ovlad.zaznamHrySkok(f.barva, f.startY, f.startX, y, x, y - 1, x - 1);
        } else if (f.startY > sloupec) {
            while (hraci[x + 1][y - 1] == null) {
                x++;
                y--;
            }
            x++;
            y--;
            this.hraci[ radek][sloupec] = null;

            this.hraci[ x][y] = null;
            this.hraci[f.startX][f.startY] = null;
            x = x + 1;
            y = y - 1;
            this.hraci[x/*-1*/][y/*-1*/] = new FigurkaD(x, y, f.barva, f.druh);

            ovlad.zaznamHrySkok(f.barva, f.startY, f.startX, y, x, y + 1, x - 1);
        }
    }

    public void damaZadaneVetsi(FigurkaD f, int sloupec, int radek, boolean dlouhySkok) throws IOException {
        int x = f.startX, y = f.startY;
        if ((f.startY + 1 == sloupec || f.startY - 1 == sloupec) || dlouhySkok) {
            // this.hraci[radek/*-1*/][sloupec/*-1*/] = new FigurkaD(f.startX, f.startY, f.barva, f.druh);
            this.hraci[f.startX][f.startY] = null;
            this.figurkaNaPoli(sloupec, radek).startX = radek;
            this.figurkaNaPoli(sloupec, radek).startY = sloupec;

            ovlad.zaznamHry(f.barva, f.startY, f.startX, sloupec, radek);
        } else if (f.startY < sloupec) {
            while (hraci[x - 1][y + 1] == null) {
                x--;
                y++;
            }
            x--;
            y++;
            this.hraci[ radek][sloupec] = null;
            this.hraci[ x][y] = null;
            this.hraci[f.startX][f.startY] = null;
            x = x - 1;
            y = y + 1;

            this.hraci[x/*-1*/][y/*-1*/] = new FigurkaD(x, y, f.barva, f.druh);

            ovlad.zaznamHrySkok(f.barva, f.startY, f.startX, y, x, y - 1, x + 1);
        } else if (f.startY > sloupec) {
            while (hraci[x - 1][y - 1] == null) {
                x--;
                y--;
            }
            x--;
            y--;
            this.hraci[ radek][sloupec] = null;

            this.hraci[ x][y] = null;
            this.hraci[f.startX][f.startY] = null;
            x = x - 1;
            y = y - 1;
            this.hraci[x/*-1*/][y/*-1*/] = new FigurkaD(x, y, f.barva, f.druh);

            ovlad.zaznamHrySkok(f.barva, f.startY, f.startX, y, x, y + 1, x + 1);
        }
    }
    /*Vypíše šachovnici*/
    /*  public int[][] vypisSachovnici() {
     int[][] vypis = new int[24][5];

     int i = 0;
     for (int a = 0; a < 8; a++) {
     for (int b = 0; b < 8; b++) {
     if (this.hraci[a][b] != null) {
     vypis[i][0] = this.hraci[a][b].startX;
     vypis[i][1] = this.hraci[a][b].startY;
     vypis[i][2] = this.hraci[a][b].barva;
     vypis[i][3] = this.hraci[a][b].druh;
     i++;
     }

     }
     }
     for (int j = 0; j < 24; j++) {
     if (vypis[j][0] == 0 && vypis[j][1] == 0) {
     vypis[j][2] = 3;
     }
     }
     return vypis;
     }*/

    //->>>
    /*prověřuje okolí figurky*/
    /* public int[][][] okoli(int sloupec, int radek, int hrac) {

     int rozdilR, rozdilS;
     int[][][] XY = new int[2][3][3];
     int i = 0, j = 0, m = 1;

     for (int rad = radek - m; rad <= radek + m; rad++) {
     for (int slo = sloupec - m; slo <= sloupec + m; slo++) {
     rozdilR = Math.abs(radek - rad);
     rozdilS = Math.abs(sloupec - slo);

     if (rozdilR == 1 && rozdilS == 1) {
     XY[0][i][j] = rad;
     XY[1][i][j] = slo;
     } else if (rozdilR == 0 && rozdilS == 1 || rozdilR == 1 && rozdilS == 0) {
     XY[0][i][j] = 11;
     XY[1][i][j] = 13;
     }

     if (j < 2) {
     j++;
     } else {
     j = 0;
     }
     }
     if (i < 2) {
     i++;
     } else {
     i = 0;
     }
     }

     return XY;

     }  /* */
    /*
     * Zjištění prázdného pole na souřadnicích
     */
    public boolean poleJePrazdne(int sloupec, int radek, int i) throws IOException {
        try {
            return this.hraci[radek][sloupec] == null;
        } catch (Exception e) {
            if (i == 1) {
                return true;
            } else {
                return false;
            }
        }
    }

    /*Vypíše eachovnici*/
    @Override
    public String toString() {
        char[] prevod = new char[]{'P', 'D'};
        char[] prevod2 = new char[]{'B', 'C'};
        char[] prevodSloupce = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
        for (int i = 7; i > -1; i--) {
            System.out.println("-----------------------------------------");
            for (int j = 0; j < 8; j++) {
                if (hraci[i][j] == null) {
                    System.out.printf("|    ");
                } else {
                    System.out.printf("| " + prevod[hraci[i][j].druh] + prevod2[hraci[i][j].barva] + " ");
                }
            }
            System.out.printf("| " + (i + 1) + "\n");
        }
        System.out.println("-----------------------------------------");
        for (int k = 0; k < 8; k++) {
            System.out.printf("  " + prevodSloupce[k] + "  ");
        }
        System.out.println(" \n");
        return "";
    }
}
